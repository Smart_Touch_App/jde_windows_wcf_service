﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ClientToSqlServer
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IClientToSqlServer" in both code and config file together.
    [ServiceContract]
    public interface IClientToSqlServer
    {
        [OperationContract]
        void DoWork();
    }
}
