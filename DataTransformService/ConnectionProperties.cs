﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
// Abstract class architecture designed by Marimuthu
//Connection related properties
namespace DataTransformService
{
    class ConnectionProperties
    {
        public string DbDataSource { get; set; }
        public string DbInitialCatalog { get; set; }
        public string DbUserID { get; set; }
        public string DbPassword { get; set; }
        public int DbConnectTimeout { get; set; }
        public bool DbIntegratedSecurity { get; set; }
    }

}
