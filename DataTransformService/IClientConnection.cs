﻿using DataTransformService.DTService;
using System.Collections.Generic;

//Interface designed by Marimuthu
namespace DataTransformService
{
    interface IClientConnection
    {
        DTService.ConnectionProperties GetConnectionDetails(string EnvironmentName);
        DTService.ConnectionProperties GetSystemConnectionDetails();
        List<string> GetEnvnameList(DataTransformService.DTService.ConnectionProperties model);
        List<DataTransformService.DTService.ConditionalParameters> GetConditionalParameters(DataTransformService.DTService.ConnectionProperties model, DataTransformService.DTService.ConditionalParameters clsParameters);
        string ProcessReplenishment(DataTransformService.DTService.ConnectionProperties model);
        string STOTProcessReplenishment(DataTransformService.DTService.ConnectionProperties model);
        string UpdateSTOTReplnsStatus(DataTransformService.DTService.ConnectionProperties model, string jdeRefSource);
        string UpdateSTOTReceipts(DataTransformService.DTService.ConnectionProperties model, string jdeRefSource);
        string GetRefSource(string environmentName);
        string ProcessMetric(DataTransformService.DTService.ConnectionProperties model);
        string ProcessDeliveryCycles(DataTransformService.DTService.ConnectionProperties model);
        string InventoryDetail(DataTransformService.DTService.ConnectionProperties model);
        string ProcessSaleOrder(DataTransformService.DTService.ConnectionProperties model);
        string ProcessReceipts(DataTransformService.DTService.ConnectionProperties model);
        List<DataTransformService.DTService.ModuleSequence> GetJobSequenceList(DataTransformService.DTService.ConnectionProperties model, int jodid);
        DataTransformService.DTService.EmailConfigDetails GetEmailConfig(DataTransformService.DTService.ConnectionProperties model, int Emailid);
        void UpdateModuleStatus(string subModuleName, string ModuleName,string JDEInterfaceID, string EnvironmentName, string status, DataTransformService.DTService.ConnectionProperties model);
        void UpdateScheduleStatusDetail(DataTransformService.DTService.ConnectionProperties model, string ServiceName, string JDEInterfaceID, string EnvironmentName, string status, string ScheduleStatus);
        void InsertServiceHistory(DataTransformService.DTService.ConnectionProperties model, int JobID, int ScheduleID, string EnvironmentName, string status);
        void UpdateJDEStatus(DataTransformService.DTService.ConnectionProperties model);
    }
}
