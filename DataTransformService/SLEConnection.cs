﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using DataTransformService.DTService;
using System.Configuration;

//// SLE Connection class architecture designed by Marimuthu
namespace DataTransformService
{
    class SLEConnection : IClientConnection
    {
        // Getting the DB Details of DV/PYU
        public DataTransformService.DTService.ConnectionProperties GetConnectionDetails(string EnvironmentName)
        {
            WriteLog("SLE Get Connection Details Called");
            DataTransformService.DTService.ConnectionProperties SLEConnectionProperties = new DTService.ConnectionProperties();
            try
            {
                switch (EnvironmentName)
                {
                    case "Prototype-User":
                        SLEConnectionProperties.DbDataSource = System.Configuration.ConfigurationManager.AppSettings["UserSLEDbDataSource"];
                        SLEConnectionProperties.DbInitialCatalog = System.Configuration.ConfigurationManager.AppSettings["UserSLEDbInitialCatalog"];
                        SLEConnectionProperties.DbUserID = System.Configuration.ConfigurationManager.AppSettings["UserSLEDbUserID"];
                        SLEConnectionProperties.DbPassword = System.Configuration.ConfigurationManager.AppSettings["UserSLEDbPassword"];
                        SLEConnectionProperties.DbConnectTimeout = int.Parse(System.Configuration.ConfigurationManager.AppSettings["UserSLEDbConnectTimeout"]);
                        SLEConnectionProperties.JDEWriteLinkSource = System.Configuration.ConfigurationManager.AppSettings["UserSLEDbIntegratedSecurity"];
                        break;
                    case "Prototype-DV":
                        SLEConnectionProperties.DbDataSource = System.Configuration.ConfigurationManager.AppSettings["DVSLEDbDataSource"];
                        SLEConnectionProperties.DbInitialCatalog = System.Configuration.ConfigurationManager.AppSettings["DVSLEDbInitialCatalog"];
                        SLEConnectionProperties.DbUserID = System.Configuration.ConfigurationManager.AppSettings["DVSLEDbUserID"];
                        SLEConnectionProperties.DbPassword = System.Configuration.ConfigurationManager.AppSettings["DVSLEDbPassword"];
                        SLEConnectionProperties.DbConnectTimeout = int.Parse(System.Configuration.ConfigurationManager.AppSettings["DVSLEDbConnectTimeout"]);
                        SLEConnectionProperties.JDEWriteLinkSource = System.Configuration.ConfigurationManager.AppSettings["DVSLEDbIntegratedSecurity"];
                        break;
                    case "Production-PD":
                        SLEConnectionProperties.DbDataSource = System.Configuration.ConfigurationManager.AppSettings["DVSLEDbDataSource"];
                        SLEConnectionProperties.DbInitialCatalog = System.Configuration.ConfigurationManager.AppSettings["DVSLEDbInitialCatalog"];
                        SLEConnectionProperties.DbUserID = System.Configuration.ConfigurationManager.AppSettings["DVSLEDbUserID"];
                        SLEConnectionProperties.DbPassword = System.Configuration.ConfigurationManager.AppSettings["DVSLEDbPassword"];
                        SLEConnectionProperties.DbConnectTimeout = int.Parse(System.Configuration.ConfigurationManager.AppSettings["DVSLEDbConnectTimeout"]);
                        SLEConnectionProperties.JDEWriteLinkSource = System.Configuration.ConfigurationManager.AppSettings["DVSLEDbIntegratedSecurity"];
                        break;
                    case "Prototype-DevTeam":
                        SLEConnectionProperties.DbDataSource = System.Configuration.ConfigurationManager.AppSettings["PYDSLEDbDataSource"];
                        SLEConnectionProperties.DbInitialCatalog = System.Configuration.ConfigurationManager.AppSettings["PYDSLEDbInitialCatalog"];
                        SLEConnectionProperties.DbUserID = System.Configuration.ConfigurationManager.AppSettings["PYDSLEDbUserID"];
                        SLEConnectionProperties.DbPassword = System.Configuration.ConfigurationManager.AppSettings["PYDSLEDbPassword"];
                        SLEConnectionProperties.DbConnectTimeout = int.Parse(System.Configuration.ConfigurationManager.AppSettings["PYDSLEDbConnectTimeout"]);
                        SLEConnectionProperties.JDEWriteLinkSource = System.Configuration.ConfigurationManager.AppSettings["PYDSLEDbIntegratedSecurity"];
                        break;
                    default:
                        SLEConnectionProperties = null;
                        break;
                }
                return SLEConnectionProperties;
            }
            catch (Exception ex)
            {
                Errorlog("[DataTransformService].[SLEConnection].[GetConnectionDetails] Error Message:" + ex.Message + "  Error StackTrace" + ex.StackTrace);
            }
            return SLEConnectionProperties;
        }


        public DataTransformService.DTService.ConnectionProperties GetSystemConnectionDetails()
        {
            WriteLog("System DB Connection Details Called");
            DataTransformService.DTService.ConnectionProperties SLESystemConnectionProperties = new DTService.ConnectionProperties();
            try
            {
                SLESystemConnectionProperties.DbDataSource = System.Configuration.ConfigurationManager.AppSettings["SystemDbDataSource"];
                SLESystemConnectionProperties.DbInitialCatalog = System.Configuration.ConfigurationManager.AppSettings["SystemDbInitialCatalog"];
                SLESystemConnectionProperties.DbUserID = System.Configuration.ConfigurationManager.AppSettings["SystemDbUserID"];
                SLESystemConnectionProperties.DbPassword = System.Configuration.ConfigurationManager.AppSettings["SystemDbPassword"];
                SLESystemConnectionProperties.DbConnectTimeout = int.Parse(System.Configuration.ConfigurationManager.AppSettings["SystemDbConnectTimeout"]); 
                SLESystemConnectionProperties.JDEWriteLinkSource = System.Configuration.ConfigurationManager.AppSettings["SystemDbIntegratedSecurity"]; 
                return SLESystemConnectionProperties;
            }
            catch (Exception ex)
            {
                Errorlog("[DataTransformService].[SLEConnection].[GetSystemConnectionDetails] Error Message:" + ex.Message + "  Error StackTrace" + ex.StackTrace);
            }
            return SLESystemConnectionProperties;
        }
        public List<DataTransformService.DTService.ModuleSequence> GetJobSequenceList(DataTransformService.DTService.ConnectionProperties model, int jodid)
        {
            List<DataTransformService.DTService.ModuleSequence> moduleSeq = null;
            try
            {
                WriteLog("[DataTransformService].[SLEConnection] [Start:GetJobSequenceList]");
                SLEJDEInterfaceClient SLE = new SLEJDEInterfaceClient();
                moduleSeq = SLE.GetJobSequenceListDetail(model, jodid).ToList();
                WriteLog("[DataTransformService].[SLEConnection] [End:GetJobSequenceList]");
            }
            catch (Exception ex)
            {
                Errorlog("[DataTransformService].[SLEConnection].[GetJobSequenceList] Error Message:" + ex.Message + "  Error StackTrace" + ex.StackTrace);
            }
            return moduleSeq;
        }
        public EmailConfigDetails GetEmailConfig(DataTransformService.DTService.ConnectionProperties model, int Emailid)
        {
            EmailConfigDetails EmailConfig = null;
            try
            {
                WriteLog("[DataTransformService].[SLEConnection] [Start:GetEmailConfig]");
                SLEJDEInterfaceClient SLE = new SLEJDEInterfaceClient();
                EmailConfig = SLE.GetEmailConfigDetail(model, Emailid);
                WriteLog("[DataTransformService].[SLEConnection] [End:GetEmailConfig]");
            }
            catch (Exception ex)
            {
                Errorlog("[DataTransformService].[SLEConnection].[GetJobSequenceList] Error Message:" + ex.Message + "  Error StackTrace" + ex.StackTrace);
            }
            return EmailConfig;
        }
        public List<string> GetEnvnameList(DataTransformService.DTService.ConnectionProperties model)
        {
            List<string> envnamelist = new List<string>();

            try
            {
                WriteLog("[DataTransformService].[SLEConnection] [Start:GetEnvnameList]");
                SLEJDEInterfaceClient SLE = new SLEJDEInterfaceClient();

                envnamelist = SLE.GetEnvironmentList(model).ToList();
                WriteLog("[DataTransformService].[SLEConnection] [End:GetEnvnameList]");
            }
            catch (Exception ex)
            {
                Errorlog("[DataTransformService].[SLEConnection].[GetEnvnameList] Error Message:" + ex.Message + "  Error StackTrace" + ex.StackTrace);
            }
            return new List<string>();
        }

        public string ProcessReplenishment(DataTransformService.DTService.ConnectionProperties model)
        {
            string status = null;
            try
            {
                WriteLog("[DataTransformService].[SLEConnection] [Start:ProcessReplenishment]");
                WriteLog("Entry the  Process Replenishment....");
                SLEJDEInterfaceClient SLE = new SLEJDEInterfaceClient();
                status = SLE.ProcessReplenishment(model);
                WriteLog("[DataTransformService].[SLEConnection] [End:ProcessReplenishment]");
                WriteLog("Process Replenishment End");

            }
            catch (Exception ex)
            {
                status = "\t ProcessReplenishment ERROR:\r\n \t\tError Message:\r\n \t" + ex.Message + "\r\n \t\t  Error StackTrace:\r\n \t" + ex.StackTrace;
                Errorlog("[DataTransformService].[SLEConnection].[ProcessReplenishment] Error Message:" + ex.Message + "  Error StackTrace" + ex.StackTrace);
            }
            return status;
        }

        public string STOTProcessReplenishment(DataTransformService.DTService.ConnectionProperties model)
        {
            string status = null;
            try
            {
                WriteLog("[DataTransformService].[SLEConnection] [Start:STOTProcessReplenishment]");
                WriteLog("Entry the STOT Process Replenishment....");
                SLEJDEInterfaceClient SLE = new SLEJDEInterfaceClient();
                status = SLE.STOTProcessReplenishment(model);
                WriteLog("[DataTransformService].[SLEConnection] [End:STOTProcessReplenishment]");
                WriteLog("STOT Process Replenishment End");

            }
            catch (Exception ex)
            {
                status = "\t STOTProcessReplenishment ERROR:\r\n \t\tError Message:\r\n \t" + ex.Message + "\r\n \t\t  Error StackTrace:\r\n \t" + ex.StackTrace;
                Errorlog("[DataTransformService].[SLEConnection].[STOTProcessReplenishment] Error Message:" + ex.Message + "  Error StackTrace" + ex.StackTrace);
            }
            return status;
        }
        public string UpdateSTOTReplnsStatus(DataTransformService.DTService.ConnectionProperties model, string jdeRefSource)
        {
            string status = null;
            try
            {
                WriteLog("[DataTransformService].[SLEConnection] [Start:UpdateSTOTReplnsStatus]");
                WriteLog("Entry the Status Update STOT Process Replenishment....");
                SLEJDEInterfaceClient SLE = new SLEJDEInterfaceClient();
                status = SLE.Update_STOTProcessReplenishment(model,jdeRefSource);
                WriteLog("[DataTransformService].[SLEConnection] [End:UpdateSTOTReplnsStatus]");
                WriteLog("Update STOT Process Replenishment Status End");

            }
            catch (Exception ex)
            {
                status = "\t STOTProcessReplenishment ERROR:\r\n \t\tError Message:\r\n \t" + ex.Message + "\r\n \t\t  Error StackTrace:\r\n \t" + ex.StackTrace;
                Errorlog("[DataTransformService].[SLEConnection].[UpdateSTOTReplnsStatus] Error Message:" + ex.Message + "  Error StackTrace" + ex.StackTrace);
            }
            return status;
        }
        public string UpdateSTOTReceipts(DataTransformService.DTService.ConnectionProperties model, string jdeRefSource)
        {
            string status = null;
            try
            {
                WriteLog("[DataTransformService].[SLEConnection] [Start:UpdateSTOTReplnsStatus]");
                WriteLog("Entry the Status Update STOT Process Replenishment....");
                SLEJDEInterfaceClient SLE = new SLEJDEInterfaceClient();
                status = SLE.Update_STOTProcessReceipts(model,jdeRefSource);
                WriteLog("[DataTransformService].[SLEConnection] [End:UpdateSTOTReplnsStatus]");
                WriteLog("Update STOT Process Replenishment Status End");

            }
            catch (Exception ex)
            {
                status = "\t STOTProcessReplenishment ERROR:\r\n \t\tError Message:\r\n \t" + ex.Message + "\r\n \t\t  Error StackTrace:\r\n \t" + ex.StackTrace;
                Errorlog("[DataTransformService].[SLEConnection].[UpdateSTOTReplnsStatus] Error Message:" + ex.Message + "  Error StackTrace" + ex.StackTrace);
            }
            return status;
        }
        
       public string GetRefSource(string environmentName)
        {
            string refSource = "";
           try
            {
               switch (environmentName)
                {
                    case "Prototype-User":
                        refSource = System.Configuration.ConfigurationManager.AppSettings["UserRefSource"];
                        break;
                    case "Prototype-DV":
                        refSource = System.Configuration.ConfigurationManager.AppSettings["DVRefSource"];
                        break;
                    case "Production-PD":
                        refSource = System.Configuration.ConfigurationManager.AppSettings["DVRefSource"];
                        break;
                    case "Prototype-DevTeam":
                        refSource = System.Configuration.ConfigurationManager.AppSettings["PYDRefSource"];
                        break;
                }
           }
           catch (Exception ex)  
           {
               Errorlog("[DataTransformService].[SLEConnection].[GetRefSource] Error Message:" + ex.Message + "  Error StackTrace" + ex.StackTrace);
           }
           return refSource;
        }


        public string InventoryDetail(DataTransformService.DTService.ConnectionProperties model)
        {
            string status = null;
            try
            {
                WriteLog("[DataTransformService].[SLEConnection] [Start:InventoryDetail]");
                WriteLog("Entry the  Process Inventory....");
                SLEJDEInterfaceClient SLE = new SLEJDEInterfaceClient();
                status = SLE.InsertInventoryDetail(model);
                WriteLog("[DataTransformService].[SLEConnection] [End:InventoryDetail]");
                WriteLog("Process Replenishment End");

            }
            catch (Exception ex)
            {
                status = "\t Inventory Details ERROR:\r\n \t\tError Message:\r\n \t" + ex.Message + "\r\n \t\t  Error StackTrace:\r\n \t" + ex.StackTrace;
                Errorlog("[DataTransformService].[SLEConnection].[InventoryDetail] Error Message:" + ex.Message + "  Error StackTrace" + ex.StackTrace);
            }
            return status;
        }

        public string ProcessSaleOrder(DataTransformService.DTService.ConnectionProperties model)
        {
            string status = null;
            try
            {
                WriteLog("[DataTransformService].[SLEConnection] [Start:ProcessSaleOrder]");
                WriteLog("Entry the  Process SaleOrder....");
                SLEJDEInterfaceClient SLE = new SLEJDEInterfaceClient();
                status = SLE.ProcessSaleOrder(model);
                WriteLog("[DataTransformService].[SLEConnection] [End:ProcessSaleOrder]");
                WriteLog("Process SaleOrder End.");

            }
            catch (Exception ex)
            {
                status = "\t ProcessSaleOrder ERROR:\r\n \t\tError Message:\r\n \t" + ex.Message + "\r\n \t\t  Error StackTrace:\r\n \t" + ex.StackTrace;
                Errorlog("[DataTransformService].[SLEConnection].[ProcessSaleOrder] Error Message:" + ex.Message + "  Error StackTrace" + ex.StackTrace);
            }
            return status;
        }


        public string ProcessReceipts(DataTransformService.DTService.ConnectionProperties model)
        {
            string status = null;
            try
            {
                WriteLog("[DataTransformService].[SLEConnection] [Start:ProcessReceipts]");
                WriteLog("Entry the  Process Receipts....");
                SLEJDEInterfaceClient SLE = new SLEJDEInterfaceClient();
                status = SLE.ProcessReceipts(model);
                WriteLog("[DataTransformService].[SLEConnection] [End:ProcessReceipts]");
                WriteLog("Process Receipts End.");

            }
            catch (Exception ex)
            {
                status = "\t ProcessSaleOrder ERROR:\r\n \t\tError Message:\r\n \t" + ex.Message + "\r\n \t\t  Error StackTrace:\r\n \t" + ex.StackTrace;
                Errorlog("[DataTransformService].[SLEConnection].[ProcessReceipts] Error Message:" + ex.Message + "  Error StackTrace" + ex.StackTrace);
            }
            return status;
        }
        public void UpdateJDEStatus(DataTransformService.DTService.ConnectionProperties model)
        {
            try
            {
                WriteLog("[DataTransformService].[SLEConnection] [Start:UpdateJDEStatus]");
                SLEJDEInterfaceClient SLE = new SLEJDEInterfaceClient();
                int result=SLE.UpdateJDEStatus(model);
                int resultvar = SLE.UpdateVarianceStatus(model);
                WriteLog("[DataTransformService].[SLEConnection] [End:UpdateJDEStatus]");

            }
            catch (Exception ex)
            {
                Errorlog("[DataTransformService].[SLEConnection].[UpdateJDEStatus] Error Message:" + ex.Message + "  Error StackTrace" + ex.StackTrace);
            }
        }
        public string ProcessMetric(DataTransformService.DTService.ConnectionProperties model)
        {
            string status = null;
            try
            {
                WriteLog("[DataTransformService].[SLEConnection] [Start:ProcessMetric]");
                WriteLog("Entry the  Process Metric....");
                SLEJDEInterfaceClient SLE = new SLEJDEInterfaceClient();
                status = SLE.InsertMetricDetails(model);
                WriteLog("[DataTransformService].[SLEConnection] [End:ProcessMetric]");
                WriteLog("Process Metric End");

            }
            catch (Exception ex)
            {
                status = "\t ProcessMetric ERROR:\r\n \t\tError Message:\r\n \t" + ex.Message + "\r\n \t\t  Error StackTrace:\r\n \t" + ex.StackTrace;
                Errorlog("[DataTransformService].[SLEConnection].[ProcessMetric] Error Message:" + ex.Message + "  Error StackTrace" + ex.StackTrace);
            }
            return status;
        }
        public string ProcessDeliveryCycles(DataTransformService.DTService.ConnectionProperties model)
        {
            string status = null;
            try
            {
                WriteLog("[DataTransformService].[SLEConnection] [Start:ProcessDeliveryCycles]");
                WriteLog("Entry the  Process DeliveryCycle....");
                SLEJDEInterfaceClient SLE = new SLEJDEInterfaceClient();
                status = SLE.InsertDeliveryCycles(model);
                WriteLog("[DataTransformService].[SLEConnection] [End:ProcessDeliveryCycles]");
                WriteLog("Process DeliveryCycles End");

            }
            catch (Exception ex)
            {
                status = "\t Process DeliveryCycles ERROR:\r\n \t\tError Message:\r\n \t" + ex.Message + "\r\n \t\t  Error StackTrace:\r\n \t" + ex.StackTrace;
                Errorlog("[DataTransformService].[SLEConnection].[ProcessDeliveryCycles] Error Message:" + ex.Message + "  Error StackTrace" + ex.StackTrace);
            }
            return status;
        }

        

        public List<DataTransformService.DTService.ConditionalParameters> GetConditionalParameters(DataTransformService.DTService.ConnectionProperties model, DataTransformService.DTService.ConditionalParameters clsParameters)
        {
            List<DataTransformService.DTService.ConditionalParameters> clsPara = null;
            try
            {
                WriteLog("[DataTransformService].[SLEConnection] [Start:GetConditionalParameters]");
               
                SLEJDEInterfaceClient SLE = new SLEJDEInterfaceClient();
                clsPara = SLE.GetScheduleList(model, clsParameters).ToList();
                WriteLog("[DataTransformService].[SLEConnection] [End:GetConditionalParameters]");
              
            }
            catch (Exception ex)
            {
                Errorlog("[DataTransformService].[SLEConnection].[GetConditionalParameters] Error Message:" + ex.Message + "  Error StackTrace" + ex.StackTrace);
            }
            return clsPara;
        }
        public string InsertUpdateSubModuleDetail(DataTransformService.DTService.ConnectionProperties model, string JDEInterfaceID, string subModuleName, string moduleName, string envName, string status, DataTransformService.DTService.ConditionalParameters clsParameters)
        {
            string statusret = "";
            try
            {
                WriteLog("[DataTransformService].[SLEConnection] [Start:InsertUpdateSubModuleDetail]");
               
                DataTransformService.DTService.ModuleDetail Moduledetail = new ModuleDetail();
                Moduledetail.EnvironmentName = envName;
                Moduledetail.JDEInterfaceID = JDEInterfaceID;
                Moduledetail.ErrorStatus = string.Empty;
                SLEJDEInterfaceClient SLE = new SLEJDEInterfaceClient();
                statusret = SLE.InsertUpdateSubModuleDetail(model, Moduledetail);
                WriteLog("[DataTransformService].[SLEConnection] [End:InsertUpdateSubModuleDetail]");
               
            }
            catch (Exception ex)
            {
                Errorlog("[DataTransformService].[SLEConnection].[InsertUpdateSubModuleDetail] Error Message:" + ex.Message + "  Error StackTrace" + ex.StackTrace);
            }
            return statusret;

        }


        public void UpdateScheduleStatusDetail(DataTransformService.DTService.ConnectionProperties model, string ServiceName, string JDEInterfaceID, string EnvironmentName, string status, string ScheduleStatus)
        {
            try
            {
                WriteLog("[DataTransformService].[SLEConnection] [Start:UpdateScheduleStatusDetail]");
                SLEJDEInterfaceClient SLE = new SLEJDEInterfaceClient();
                DataTransformService.DTService.ModuleDetail moddetail = new ModuleDetail();
                moddetail.JDEInterfaceID = JDEInterfaceID;
                moddetail.ScheduleStatus = status;
                moddetail.ServiceName = ServiceName;
                moddetail.EnvironmentName = EnvironmentName;
                moddetail.ErrorStatus = status;
                SLE.UpdateScheduleDetail(model, moddetail);
                WriteLog("[DataTransformService].[SLEConnection] [End:UpdateScheduleStatusDetail]");
            }
            catch (Exception ex)
            {
                Errorlog("[DataTransformService].[SLEConnection].[UpdateScheduleStatusDetail] Error Message:" + ex.Message + "  Error StackTrace" + ex.StackTrace);
            }

        }
        public void UpdateModuleStatus(string subModuleName, string ModuleName, string JDEInterfaceID, string EnvironmentName, string status, DataTransformService.DTService.ConnectionProperties model)
        {
            string statusret = "";
            try
            {
                WriteLog("[DataTransformService].[SLEConnection] [Start:UpdateModuleStatus]");
               
                DataTransformService.DTService.ModuleDetail Moduledetail = new ModuleDetail();
                Moduledetail.EnvironmentName = EnvironmentName;
                Moduledetail.JDEInterfaceID = JDEInterfaceID;
                Moduledetail.ErrorStatus = status;
                Moduledetail.ScheduleStatus = status;
                Moduledetail.SUBModuleName = subModuleName;
                SLEJDEInterfaceClient SLE = new SLEJDEInterfaceClient();
                statusret = SLE.InsertUpdateSubModuleDetail(model, Moduledetail);
                WriteLog("[DataTransformService].[SLEConnection] [End:UpdateModuleStatus]");
                
            }
            catch (Exception ex)
            {
                Errorlog("[DataTransformService].[SLEConnection].[UpdateModuleStatus] Error Message:" + ex.Message + "  Error StackTrace" + ex.StackTrace);
            }

        }

        public void InsertServiceHistory(DataTransformService.DTService.ConnectionProperties model, int JobID, int ScheduleID, string EnvironmentName, string status)
        {

            string statusret = "";
            try
            {
                WriteLog("[DataTransformService].[SLEConnection] [Start:InsertServiceHistory]");

                DataTransformService.DTService.ServiceHistory objServiceHistory = new ServiceHistory();
                objServiceHistory.EnvironmentName = EnvironmentName;
                objServiceHistory.JobID = JobID;
                objServiceHistory.ScheduleID = ScheduleID;
                objServiceHistory.Status = status;
                SLEJDEInterfaceClient SLE = new SLEJDEInterfaceClient();
                statusret = SLE.InsertServiceHistory(model, objServiceHistory);
                WriteLog("[DataTransformService].[SLEConnection] [End:InsertServiceHistory]");
               
            }
            catch (Exception ex)
            {
                Errorlog("[DataTransformService].[SLEConnection].[InsertServiceHistory] Error Message:" + ex.Message + "  Error StackTrace" + ex.StackTrace);
            }

        }




        public void WriteLog(string message)
        {
            StreamWriter sm = null;
            try
            {
                sm = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile.txt", true);
                sm.WriteLine(DateTime.Now.ToString() + ": " + message);
                sm.Flush();
                sm.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public void Errorlog(string message)
        {
            StreamWriter sm = null;
            try
            {
                sm = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\ErrorLog.txt", true);
                sm.WriteLine(DateTime.Now.ToString() + ": " + message);
                sm.Flush();
                sm.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }

   

}

