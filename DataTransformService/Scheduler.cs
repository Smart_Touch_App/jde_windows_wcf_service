﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Configuration;
using System.Net.Mail;

namespace DataTransformService
{
    public partial class Scheduler : ServiceBase, IDisposable
    {
        private Timer timerConnection = null;
        public Scheduler()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Service is the Start for the Scheduler.
        /// </summary>
        /// <param name="args"></param>
        protected override void OnStart(string[] args)
        {
            WriteLog("Service is Started");
            WriteLog("[DataTransformService][Scheduler][Start:OnStart]");
            try
            {
                CallScheduler();
            }
            catch (Exception ex)
            {
                Errorlog("[DataTransformService].[Scheduler].[OnStart] Error Message:" + ex.Message + "  Error StackTrace" + ex.StackTrace);
            }
            timerConnection = new Timer();
            this.timerConnection.Interval = Convert.ToDouble(System.Configuration.ConfigurationManager.AppSettings["TimeInterval"]); 
            this.timerConnection.Elapsed += new System.Timers.ElapsedEventHandler(this.timerConnection_tick);
            timerConnection.Enabled = true;
            WriteLog("[DataTransformService][Scheduler][End:OnStart]");
        }



        public void OnDebug()
        {
            OnStart(null);
        }
        /// <summary>
        /// Timer to ticked base on the Interval Executrd Function 
        /// </summary>
        /// <param name="Sender"></param>
        /// <param name="e"></param>
        private void timerConnection_tick(object Sender, ElapsedEventArgs e)
        {
            try
            {
                WriteLog("[DataTransformService][Scheduler][Start:timerConnection_tick]");
                CallScheduler();
            }
            catch (Exception ex)
            {
                Errorlog("[DataTransformService].[Scheduler].[timerConnection_tick] Error Message:" + ex.Message + "  Error StackTrace" + ex.StackTrace);
            }
            WriteLog("[DataTransformService][Scheduler][End:timerConnection_tick]");
        }

        /// <summary>
        /// Call the Scheduler to Execute the Replenishment and SaleOrder to JDELink.
        /// The Vignesh.S-Marimuthuk-Vignesh.D worked on this function.
        /// </summary>
        private void CallScheduler()
        {
            try
            {
                WriteLog("[DataTransformService][Scheduler][Start:CallScheduler]");
                DataTransformService.DTService.ConditionalParameters clsParameters = new DTService.ConditionalParameters();
                List<DataTransformService.DTService.ConditionalParameters> ListConditionalParameters = new List<DTService.ConditionalParameters>();
                ConnectionFactory ConnectionFactory = new ConcreteConnectionFactory();
                IClientConnection SLEConnection = ConnectionFactory.iFactoryConnection(System.Configuration.ConfigurationManager.AppSettings["ProjectName"] );
                WriteLog("Calling Get Conditional Parameters method");

                DataTransformService.DTService.ConnectionProperties SLEConnectionProperties = new DTService.ConnectionProperties();
                DataTransformService.DTService.ConnectionProperties SLESystemConnectionProperties = SLEConnection.GetSystemConnectionDetails();

                clsParameters.EnvironmentName = System.Configuration.ConfigurationManager.AppSettings["EnvName"];
                clsParameters.Servicename = System.Configuration.ConfigurationManager.AppSettings["ServiceName"];//"DTService";
                ListConditionalParameters = SLEConnection.GetConditionalParameters(SLESystemConnectionProperties, clsParameters);
                List<DataTransformService.DTService.ModuleSequence> ListSequence = null;

                if (ListConditionalParameters != null && ListConditionalParameters.Count() > 0)
                {
                    string salesstatus = String.Empty;
                    string status = String.Empty;
                    string Receiptstatus = String.Empty;
                    ListConditionalParameters = ListConditionalParameters.OrderByDescending(x => x.IsImmediate).ToList();
                    foreach (DTService.ConditionalParameters Conditionalitem in ListConditionalParameters)
                    {

                        try
                        {
                            ListSequence = SLEConnection.GetJobSequenceList(SLESystemConnectionProperties, Conditionalitem.JobID);
                            if (ListSequence != null && ListSequence.Count() > 0)
                            {
                                ListSequence = ListSequence.OrderBy(x => x.SequenceNo).ToList();
                                SLEConnectionProperties = SLEConnection.GetConnectionDetails(Conditionalitem.EnvironmentName.Trim());
                                if (SLEConnectionProperties != null)
                                {
                                    WriteLog("Updating DB before start to execute the Replenishment method");
                                    foreach (DataTransformService.DTService.ModuleSequence Seqitem in ListSequence)
                                    {
                                        switch (Seqitem.ModuleName)
                                        {
                                            case "Sales Order":
                                                WriteLog("Executing the actual Sales Order method");
                                                salesstatus = SLEConnection.ProcessSaleOrder(SLEConnectionProperties);
                                                if (salesstatus != "Completed")
                                                {
                                                    if (Seqitem.EmailNotification)
                                                    {
                                                        Emailnotification(Seqitem.EmailConfigID, status, "Reg: JDEInterface Sales Order Status", SLEConnection);
                                                    }
                                                    if (Seqitem.Dependency)
                                                    {
                                                        goto error;
                                                    }
                                                }
                                                WriteLog("Updating DB after execute the Sales Order method");
                                                break;
                                            case "Receipts":
                                                WriteLog("Executing the actual Receipts method");
                                                Receiptstatus = SLEConnection.ProcessReceipts(SLEConnectionProperties);
                                                if (Receiptstatus != "Completed")
                                                {
                                                    if (Seqitem.EmailNotification)
                                                    {
                                                        Emailnotification(Seqitem.EmailConfigID, status, "Reg: JDEInterface Receipts Status", SLEConnection);
                                                    }
                                                    if (Seqitem.Dependency)
                                                    {
                                                        goto error;
                                                    }
                                                }
                                                WriteLog("Updating DB after execute the Receipts method");
                                                break;
                                            case "Replenishment":
                                                WriteLog("Executing the actual Replenishment method");
                                                status = SLEConnection.ProcessReplenishment(SLEConnectionProperties);
                                                if (status != "Completed")
                                                {
                                                    if (Seqitem.EmailNotification)
                                                    {
                                                        Emailnotification(Seqitem.EmailConfigID, status, "Reg: JDEInterface Replenishment Status", SLEConnection);
                                                    }
                                                    if (Seqitem.Dependency)
                                                    {
                                                        goto error;
                                                    }
                                                }
                                                WriteLog("Updating DB after execute the Replenishment method");
                                                break;
                                            case "STOTReplenishment":
                                                WriteLog("Executing the actual Replenishment method");
                                                status = SLEConnection.STOTProcessReplenishment(SLEConnectionProperties);
                                                if (status != "Completed")
                                                {
                                                    if (Seqitem.EmailNotification)
                                                    {
                                                        Emailnotification(Seqitem.EmailConfigID, status, "Reg: JDEInterface  STOT Replenishment Status", SLEConnection);
                                                    }
                                                    if (Seqitem.Dependency)
                                                    {
                                                        goto error;
                                                    }
                                                }
                                                WriteLog("Updating DB after execute the Replenishment method");
                                                break;
                                            case "UpdateSTOTReplenishmentStatus":
                                                WriteLog("Executing the actual Replenishment method");
                                                status = SLEConnection.UpdateSTOTReplnsStatus(SLEConnectionProperties, SLEConnection.GetRefSource(Conditionalitem.EnvironmentName));
                                                if (status != "Completed")
                                                {
                                                    if (Seqitem.EmailNotification)
                                                    {
                                                        Emailnotification(Seqitem.EmailConfigID, status, "Reg: JDEInterface  STOT Replenishment Status", SLEConnection);
                                                    }
                                                    if (Seqitem.Dependency)
                                                    {
                                                        goto error;
                                                    }
                                                }
                                                WriteLog("Updating DB after execute the Replenishment method");
                                                break;

                                            case "STOTReceipts":
                                                WriteLog("Executing the actual STOT Receipts method");
                                                status = SLEConnection.UpdateSTOTReceipts(SLEConnectionProperties, SLEConnection.GetRefSource(Conditionalitem.EnvironmentName));
                                                if (status != "Completed")
                                                {
                                                    if (Seqitem.EmailNotification)
                                                    {
                                                        Emailnotification(Seqitem.EmailConfigID, status, "Reg: JDEInterface  STOT Receipts Status", SLEConnection);
                                                    }
                                                    if (Seqitem.Dependency)
                                                    {
                                                        goto error;
                                                    }
                                                }
                                                WriteLog("Updating DB after execute the STOT Receipts method");
                                                break;

                                            case "CustomerMetrics":
                                                WriteLog("Executing the actual Metric method");
                                                status = SLEConnection.ProcessMetric(SLEConnectionProperties);
                                                if (status != "Completed")
                                                {
                                                    if (Seqitem.EmailNotification)
                                                    {
                                                        Emailnotification(Seqitem.EmailConfigID, status, "Reg: JDEInterface Metric Status", SLEConnection);
                                                    }
                                                    if (Seqitem.Dependency)
                                                    {
                                                        goto error;
                                                    }
                                                }
                                                WriteLog("Updating DB after execute the Metric method");
                                                break;
                                            case "DeliveryCycles":
                                                WriteLog("Executing the actual DeliveryCycles method");
                                                status = SLEConnection.ProcessDeliveryCycles(SLEConnectionProperties);
                                                if (status != "Completed")
                                                {
                                                    if (Seqitem.EmailNotification)
                                                    {
                                                        Emailnotification(Seqitem.EmailConfigID, status, "Reg: JDEInterface Delivery Cycles Status", SLEConnection);
                                                    }
                                                    if (Seqitem.Dependency)
                                                    {
                                                        goto error;
                                                    }
                                                }
                                                WriteLog("Updating DB after execute the Delivery Cycles");
                                                break;
                                            case "Inventory":
                                                WriteLog("Executing the actual Inventory method");
                                                status = SLEConnection.InventoryDetail(SLEConnectionProperties);
                                                if (status != "Completed")
                                                {
                                                    if (Seqitem.EmailNotification)
                                                    {
                                                        Emailnotification(Seqitem.EmailConfigID, status, "Reg: JDEInterface Inventory Status", SLEConnection);
                                                    }
                                                    if (Seqitem.Dependency)
                                                    {
                                                        goto error;
                                                    }
                                                }
                                                WriteLog("Updating DB after execute the Inventory method");
                                                break;

                                            default:
                                                break;
                                        }
                                    }

                                error:
                                    SLEConnectionProperties = SLEConnection.GetConnectionDetails(Conditionalitem.EnvironmentName);
                                    string SLEJDEstatus = "Error";
                                    if ((string.IsNullOrEmpty(salesstatus) || salesstatus == "Completed") && (string.IsNullOrEmpty(status) || status == "Completed") && (string.IsNullOrEmpty(Receiptstatus) || Receiptstatus == "Completed"))
                                    {
                                        SLEJDEstatus = "Completed";
                                    }
                                    SLEConnection.UpdateScheduleStatusDetail(SLESystemConnectionProperties, clsParameters.Servicename, Conditionalitem.JDEInterfaceID, Conditionalitem.EnvironmentName, SLEJDEstatus, "Completed");
                                    SLEConnection.InsertServiceHistory(SLESystemConnectionProperties, Conditionalitem.JobID, Conditionalitem.ScheduleID, Conditionalitem.EnvironmentName, SLEJDEstatus);
                                }

                            }
                        }
                        catch
                        {
                            SLEConnection.UpdateScheduleStatusDetail(SLESystemConnectionProperties, clsParameters.Servicename, Conditionalitem.JDEInterfaceID, Conditionalitem.EnvironmentName, "Error", "Completed");
                        }
                    }
                }
                UpdateSOJDEStatus(SLEConnection);
            }
            catch (Exception ex)
            {
                Errorlog("[DataTransformService].[Scheduler].[CallScheduler] Error Message:" + ex.Message + "  Error StackTrace" + ex.StackTrace);
            }
            WriteLog("[DataTransformService][Scheduler][End:CallScheduler]");
        }

       // The Vignesh.S worked on this function.
        private void UpdateSOJDEStatus(IClientConnection SLEConnection)
        {
            WriteLog("[DataTransformService][Scheduler][Start:UpdateSOJDEStatus]");
            try
            {
                List<string> envnamelist = new List<string>();
                envnamelist = SLEConnection.GetEnvnameList(SLEConnection.GetSystemConnectionDetails());
                foreach (string envname in envnamelist)
                {
                    SLEConnection.UpdateJDEStatus(SLEConnection.GetConnectionDetails(envname));
                }
            }
            catch (Exception ex)
            {
                Errorlog("[DataTransformService].[Scheduler].[UpdateSOJDEStatus] Error Message:" + ex.Message + "  Error StackTrace" + ex.StackTrace);
            }
            WriteLog("[DataTransformService][Scheduler][End:UpdateSOJDEStatus]");
        }
       //  Vignesh.S worked on this function.
        private void Emailnotification(int emailid, string Body, string Subject, IClientConnection SLEConnection)
        {
            try
            {
                WriteLog("[DataTransformService][Scheduler][Start:Emailnotification]");
                DataTransformService.DTService.EmailConfigDetails emildetails= SLEConnection.GetEmailConfig(SLEConnection.GetSystemConnectionDetails(), emailid);
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient();
                string password = Decryptdata(emildetails.password);
                mail.From = new MailAddress(emildetails.FromRecipients);
                string [] ToRecipients = emildetails.ToRecipients.Split(',');
                string[] CcRecipients = emildetails.CcRecipients.Split(',');
                string[] BccRecipients = emildetails.BccRecipients.Split(',');
                foreach (string ToRecipient in ToRecipients)
                {
                    if (!string.IsNullOrEmpty(ToRecipient))
                        mail.To.Add(new MailAddress(ToRecipient));
                }
                foreach (string CcRecipient in CcRecipients)
                {
                    if (!string.IsNullOrEmpty(CcRecipient))
                        mail.CC.Add(new MailAddress(CcRecipient));
                }
                foreach (string BccRecipient in BccRecipients)
                {
                    if (!string.IsNullOrEmpty(BccRecipient))
                        mail.CC.Add(new MailAddress(BccRecipient));
                }
                mail.Subject = Subject;
                mail.Body = Body;
                SmtpServer.Host = System.Configuration.ConfigurationManager.AppSettings["Host"];
                SmtpServer.Port = 25;
                SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                SmtpServer.UseDefaultCredentials = false;
                SmtpServer.Timeout = 10000;
                SmtpServer.Credentials = new System.Net.NetworkCredential(emildetails.FromRecipients, password);
                SmtpServer.Send(mail);
            }
            catch (Exception ex)
            {
                Errorlog("[DataTransformService].[Scheduler].[Emailnotification] Error Message:" + ex.Message + "  Error StackTrace" + ex.StackTrace);
            }
            WriteLog("[DataTransformService][Scheduler][End:Emailnotification]");
        }
        //Decryption
        // Vignesh.S and Marimuthuk created for decrypt.
        public string Decryptdata(string encryptpwd)
        {
            string decryptpwd = string.Empty;
            try
            {
               
                UTF8Encoding encodepwd = new UTF8Encoding();
                Decoder Decode = encodepwd.GetDecoder();
                byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
                int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
                char[] decoded_char = new char[charCount];
                Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
                decryptpwd = new String(decoded_char);
                
            }
            catch (Exception ex)
            {
                Errorlog("[DataTransformService].[Scheduler].[Decryptdata] Error Message:" + ex.Message + "  Error StackTrace" + ex.StackTrace);
            }
            return decryptpwd;
        }

        /// <summary>
        /// To Write the log into the TextFile.
        /// </summary>
        /// <param name="message"></param>
        public void WriteLog(string message)
        {
            StreamWriter sm = null;
            try
            {
                sm = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile.txt", true);
                sm.WriteLine(DateTime.Now.ToString() + ": " + message);
                sm.Flush();
                sm.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// To Stopped the Service .
        /// Marimuthu and Vignesh worked on this.
        /// </summary>
        protected override void OnStop()
        {
            WriteLog("Service is Stopped");
            WriteLog("[DataTransformService][Scheduler][Start:OnStop]");
            timerConnection.Enabled = false;
            WriteLog("[DataTransformService][Scheduler][End:OnStop]");
        }


        /// <summary>
        /// To Write the Errorlog into the TextFile.
        /// Vignesh worked on this.
        /// </summary>
        /// <param name="message">Error Message and Error StackTrace </param>
        public void Errorlog(string message)
        {
            StreamWriter sm = null;
            try
            {
                sm = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\ErrorLog.txt", true);
                sm.WriteLine(DateTime.Now.ToString() + ": " + message);
                sm.Flush();
                sm.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
