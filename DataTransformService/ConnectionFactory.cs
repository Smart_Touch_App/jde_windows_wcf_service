﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
// Abstract class architecture designed by Marimuthu
namespace DataTransformService
{
       abstract class ConnectionFactory
        {
            public abstract IClientConnection iFactoryConnection(string clientType);
        }

           class ConcreteConnectionFactory : ConnectionFactory
           {
               public override IClientConnection iFactoryConnection(string clientType)
               {
                   try
                   {
                       switch (clientType)
                       {
                           case "SLE":
                               return new SLEConnection();

                           default:
                               throw new ApplicationException(string.Format("Unable to Establish the connection"));
                       }
                   }
                   catch (Exception factoryEx)
                   {
                       throw factoryEx;
                   
                   }
               }
           }
}
