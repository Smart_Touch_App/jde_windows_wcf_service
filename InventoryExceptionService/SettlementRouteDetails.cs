﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryExceptionService
{
    class SettlementRouteDetails
    {
      
            public string FromDate { get; set; }
            public string ToDate { get; set; }
            public string RouteIDs { get; set; }
      
    }
}
