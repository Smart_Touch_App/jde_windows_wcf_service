﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryExceptionService
{
    interface IClientConnection
    {
        DTService.ConnectionProperties GetConnectionDetails(string EnvironmentName);
        DTService.ConnectionProperties GetSystemConnectionDetails();
        List<InventoryExceptionService.DTService.SettlementDetails> GetSettlementDetails(InventoryExceptionService.DTService.ConnectionProperties model);
        List<string> GetEnvnameList(InventoryExceptionService.DTService.ConnectionProperties model);
        List<InventoryExceptionService.DTService.ConditionalParameters> GetConditionalParameters(InventoryExceptionService.DTService.ConnectionProperties model, InventoryExceptionService.DTService.ConditionalParameters clsParameters);
        List<InventoryExceptionService.DTService.ModuleSequence> GetJobSequenceList(InventoryExceptionService.DTService.ConnectionProperties model, int jodid);
        InventoryExceptionService.DTService.EmailConfigDetails GetEmailConfig(InventoryExceptionService.DTService.ConnectionProperties model, int Emailid);
        void UpdateModuleStatus(string subModuleName, string ModuleName, string JDEInterfaceID, string EnvironmentName, string status, InventoryExceptionService.DTService.ConnectionProperties model);
        void UpdateScheduleStatusDetail(InventoryExceptionService.DTService.ConnectionProperties model, string ServiceName, string JDEInterfaceID, string EnvironmentName, string status, string ScheduleStatus);
        void InsertServiceHistory(InventoryExceptionService.DTService.ConnectionProperties model, int JobID, int ScheduleID, string EnvironmentName, string status);
        string ProcessInventoryException(InventoryExceptionService.DTService.ConnectionProperties model);
        
    }
}
