﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace InventoryExceptionService
{
    public partial class InventoryException : ServiceBase, IDisposable
    {
        private Timer timerConnection = null;
        public InventoryException()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            WriteLog("Service is Started");
            WriteLog("[InventoryException][Scheduler][Start:OnStart]");
            try
            {
                CallScheduler();
            }
            catch (Exception ex)
            {
                Errorlog("[InventoryException].[Scheduler].[OnStart] Error Message:" + ex.Message + "  Error StackTrace" + ex.StackTrace);
            }
            timerConnection = new Timer();
            this.timerConnection.Interval = 300000;
            this.timerConnection.Elapsed += new System.Timers.ElapsedEventHandler(this.timerConnection_tick);
            timerConnection.Enabled = true;
            WriteLog("[InventoryException][Scheduler][End:OnStart]");
        }


        public void OnDebug()
        {
            OnStart(null);
        }
        /// <summary>
        /// Timer to ticked base on the Interval Executrd Function 
        /// </summary>
        /// <param name="Sender"></param>
        /// <param name="e"></param>
        private void timerConnection_tick(object Sender, ElapsedEventArgs e)
        {
            try
            {
                WriteLog("[InventoryException][Scheduler][Start:timerConnection_tick]");
                CallScheduler();
            }
            catch (Exception ex)
            {
                Errorlog("[InventoryException].[Scheduler].[timerConnection_tick] Error Message:" + ex.Message + "  Error StackTrace" + ex.StackTrace);
            }
            WriteLog("[InventoryException][Scheduler][End:timerConnection_tick]");
        }


        private void CallScheduler()
        {
            try
            {
                WriteLog("[DataTransformService][InventoryException][Start:CallScheduler]");
                InventoryExceptionService.DTService.ConditionalParameters clsParameters = new DTService.ConditionalParameters();
                List<InventoryExceptionService.DTService.ConditionalParameters> ListConditionalParameters = new List<DTService.ConditionalParameters>();
                ConnectionFactory ConnectionFactory = new ConcreteConnectionFactory();
                IClientConnection SLEConnection = ConnectionFactory.iFactoryConnection("SLE");
                WriteLog("Calling Get Conditional Parameters method");

                InventoryExceptionService.DTService.ConnectionProperties SLEConnectionProperties = new DTService.ConnectionProperties();
                InventoryExceptionService.DTService.ConnectionProperties SLESystemConnectionProperties = SLEConnection.GetSystemConnectionDetails();

                clsParameters.EnvironmentName = System.Configuration.ConfigurationManager.AppSettings["EnvName"];//"Prototype-DV";
                clsParameters.Servicename = System.Configuration.ConfigurationManager.AppSettings["ServiceName"];//"DTService";
                ListConditionalParameters = SLEConnection.GetConditionalParameters(SLESystemConnectionProperties, clsParameters);
                List<InventoryExceptionService.DTService.ModuleSequence> ListSequence = null;

                if (ListConditionalParameters != null && ListConditionalParameters.Count() > 0)
                {
                    WriteLog("Executing Conditional Parameters");
                    string salesstatus = "";
                    string status = "";
                    string Receiptstatus = "";
                    ListConditionalParameters = ListConditionalParameters.OrderByDescending(x => x.IsImmediate).ToList();
                    foreach (DTService.ConditionalParameters Conditionalitem in ListConditionalParameters)
                    {

                        try
                        {
                            ListSequence = SLEConnection.GetJobSequenceList(SLESystemConnectionProperties, Conditionalitem.JobID);
                            if (ListSequence != null && ListSequence.Count() > 0)
                            {
                                ListSequence = ListSequence.OrderBy(x => x.SequenceNo).ToList();
                                SLEConnectionProperties = SLEConnection.GetConnectionDetails(Conditionalitem.EnvironmentName.Trim());

                                //List<string> LstSettlementDetails = new List<string>();
                                //LstSettlementDetails = SLEConnection.GetSettlementDetails(SLEConnectionProperties);

                                if (SLEConnectionProperties != null)
                                {
                                    WriteLog("Updating DB before start to execute the Inventory Exception method");
                                    foreach (InventoryExceptionService.DTService.ModuleSequence Seqitem in ListSequence)
                                    {
                                        switch (Seqitem.ModuleName)
                                        {
                                            case "InventoryException":
                                                WriteLog("Executing the actual Inventory Exception method");

                                                salesstatus = SLEConnection.ProcessInventoryException(SLEConnectionProperties);
                                                if (salesstatus != "Completed")
                                                {
                                                    if (Seqitem.EmailNotification)
                                                    {
                                                        Emailnotification(Seqitem.EmailConfigID, status, "Reg: JDEInterface Inventory Exception Status", SLEConnection);
                                                    }
                                                    if (Seqitem.Dependency)
                                                    {
                                                        goto error;
                                                    }
                                                }
                                                WriteLog("Updating DB after execute the InventoryException method");
                                                break;
                                            
                                            default:
                                                break;
                                        }
                                    }

                                error:
                                    SLEConnectionProperties = SLEConnection.GetConnectionDetails(Conditionalitem.EnvironmentName);
                                    string SLEJDEstatus = "Error";
                                    if ((string.IsNullOrEmpty(salesstatus) || salesstatus == "Completed") && (string.IsNullOrEmpty(status) || status == "Completed") && (string.IsNullOrEmpty(Receiptstatus) || Receiptstatus == "Completed"))
                                    {
                                        SLEJDEstatus = "Completed";
                                    }
                                    SLEConnection.UpdateScheduleStatusDetail(SLESystemConnectionProperties, clsParameters.Servicename, Conditionalitem.JDEInterfaceID, Conditionalitem.EnvironmentName, SLEJDEstatus, "Completed");
                                    SLEConnection.InsertServiceHistory(SLESystemConnectionProperties, Conditionalitem.JobID, Conditionalitem.ScheduleID, Conditionalitem.EnvironmentName, SLEJDEstatus);
                                }

                            }
                        }
                        catch
                        {
                            SLEConnection.UpdateScheduleStatusDetail(SLESystemConnectionProperties, clsParameters.Servicename, Conditionalitem.JDEInterfaceID, Conditionalitem.EnvironmentName, "Error", "Completed");
                        }
                    }
                }
                
            }
            catch (Exception ex)
            {
                Errorlog("[DataTransformService].[InventoryException].[CallScheduler] Error Message:" + ex.Message + "  Error StackTrace" + ex.StackTrace);
            }
            WriteLog("[DataTransformService][InventoryException][End:CallScheduler]");
        }

        protected override void OnStop()
        {
            WriteLog("Service is Stopped");
        }

  private void Emailnotification(int emailid, string Body, string Subject, IClientConnection SLEConnection)
        {
            try
            {
                WriteLog("[DataTransformService][InventoryException][Start:Emailnotification]");
                InventoryExceptionService.DTService.EmailConfigDetails emildetails= SLEConnection.GetEmailConfig(SLEConnection.GetSystemConnectionDetails(), emailid);
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient();
                string password = Decryptdata(emildetails.password);
                mail.From = new MailAddress(emildetails.FromRecipients);
                string [] ToRecipients = emildetails.ToRecipients.Split(',');
                string[] CcRecipients = emildetails.CcRecipients.Split(',');
                string[] BccRecipients = emildetails.BccRecipients.Split(',');
                foreach (string ToRecipient in ToRecipients)
                {
                    if (!string.IsNullOrEmpty(ToRecipient))
                        mail.To.Add(new MailAddress(ToRecipient));
                }
                foreach (string CcRecipient in CcRecipients)
                {
                    if (!string.IsNullOrEmpty(CcRecipient))
                        mail.CC.Add(new MailAddress(CcRecipient));
                }
                foreach (string BccRecipient in BccRecipients)
                {
                    if (!string.IsNullOrEmpty(BccRecipient))
                        mail.CC.Add(new MailAddress(BccRecipient));
                }
                mail.Subject = Subject;
                mail.Body = Body;
                SmtpServer.Host = System.Configuration.ConfigurationManager.AppSettings["Host"];
                SmtpServer.Port = 25;
                SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                SmtpServer.UseDefaultCredentials = false;
                SmtpServer.Timeout = 10000;
                SmtpServer.Credentials = new System.Net.NetworkCredential(emildetails.FromRecipients, password);
                SmtpServer.Send(mail);
            }
            catch (Exception ex)
            {
                Errorlog("[DataTransformService].[InventoryException].[Emailnotification] Error Message:" + ex.Message + "  Error StackTrace" + ex.StackTrace);
            }
            WriteLog("[DataTransformService][InventoryException][End:Emailnotification]");
        }
        //Decryption
        public string Decryptdata(string encryptpwd)
        {
            string decryptpwd = string.Empty;
            UTF8Encoding encodepwd = new UTF8Encoding();
            Decoder Decode = encodepwd.GetDecoder();
            byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
            int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
            char[] decoded_char = new char[charCount];
            Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
            decryptpwd = new String(decoded_char);
            return decryptpwd;
        }




        /// <summary>
        /// To Write the log into the TextFile.
        /// </summary>
        /// <param name="message"></param>
        public void WriteLog(string message)
        {
            StreamWriter sm = null;
            try
            {
                sm = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile.txt", true);
                sm.WriteLine(DateTime.Now.ToString() + ": " + message);
                sm.Flush();
                sm.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// To Write the Errorlog into the TextFile.
        /// </summary>
        /// <param name="message">Error Message and Error StackTrace </param>
        public void Errorlog(string message)
        {
            StreamWriter sm = null;
            try
            {
                sm = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\ErrorLog.txt", true);
                sm.WriteLine(DateTime.Now.ToString() + ": " + message);
                sm.Flush();
                sm.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
