﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryExceptionService
{
    abstract class ConnectionFactory
    {
        public abstract IClientConnection iFactoryConnection(string clientType);
    }

    class ConcreteConnectionFactory : ConnectionFactory
    {
        public override IClientConnection iFactoryConnection(string clientType)
        {
            switch (clientType)
            {
                case "SLE":
                    return new SLEConnection();

                default:
                    throw new ApplicationException(string.Format("Unable to Establish the connection"));
            }
        }
    }
}
