﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using InventoryExceptionService.DTService;
using System.Text;
using System.Threading.Tasks;

namespace InventoryExceptionService
{
    class SLEConnection : IClientConnection
    {
        // Getting the DB Details of DV/PYU
        public InventoryExceptionService.DTService.ConnectionProperties GetConnectionDetails(string EnvironmentName)
        {
            WriteLog("SLE Get Connection Details Called");
            InventoryExceptionService.DTService.ConnectionProperties SLEConnectionProperties = new DTService.ConnectionProperties();
            try
            {
                switch (EnvironmentName)
                {
                    case "Prototype-User":
                        SLEConnectionProperties.DbDataSource = System.Configuration.ConfigurationManager.AppSettings["UserSLEDbDataSource"];
                        SLEConnectionProperties.DbInitialCatalog = System.Configuration.ConfigurationManager.AppSettings["UserSLEDbInitialCatalog"];
                        SLEConnectionProperties.DbUserID = System.Configuration.ConfigurationManager.AppSettings["UserSLEDbUserID"];
                        SLEConnectionProperties.DbPassword = System.Configuration.ConfigurationManager.AppSettings["UserSLEDbPassword"];
                        SLEConnectionProperties.DbConnectTimeout = int.Parse(System.Configuration.ConfigurationManager.AppSettings["UserSLEDbConnectTimeout"]);
                        SLEConnectionProperties.JDEWriteLinkSource = System.Configuration.ConfigurationManager.AppSettings["UserSLEDbIntegratedSecurity"];
                        break;
                    case "Prototype-DV":
                        SLEConnectionProperties.DbDataSource = System.Configuration.ConfigurationManager.AppSettings["DVSLEDbDataSource"];
                        SLEConnectionProperties.DbInitialCatalog = System.Configuration.ConfigurationManager.AppSettings["DVSLEDbInitialCatalog"];
                        SLEConnectionProperties.DbUserID = System.Configuration.ConfigurationManager.AppSettings["DVSLEDbUserID"];
                        SLEConnectionProperties.DbPassword = System.Configuration.ConfigurationManager.AppSettings["DVSLEDbPassword"];
                        SLEConnectionProperties.DbConnectTimeout = int.Parse(System.Configuration.ConfigurationManager.AppSettings["DVSLEDbConnectTimeout"]);
                        SLEConnectionProperties.JDEWriteLinkSource = System.Configuration.ConfigurationManager.AppSettings["DVSLEDbIntegratedSecurity"];
                        break;
                    case "Production-PD":
                        SLEConnectionProperties.DbDataSource = System.Configuration.ConfigurationManager.AppSettings["DVSLEDbDataSource"];
                        SLEConnectionProperties.DbInitialCatalog = System.Configuration.ConfigurationManager.AppSettings["DVSLEDbInitialCatalog"];
                        SLEConnectionProperties.DbUserID = System.Configuration.ConfigurationManager.AppSettings["DVSLEDbUserID"];
                        SLEConnectionProperties.DbPassword = System.Configuration.ConfigurationManager.AppSettings["DVSLEDbPassword"];
                        SLEConnectionProperties.DbConnectTimeout = int.Parse(System.Configuration.ConfigurationManager.AppSettings["DVSLEDbConnectTimeout"]);
                        SLEConnectionProperties.JDEWriteLinkSource = System.Configuration.ConfigurationManager.AppSettings["DVSLEDbIntegratedSecurity"];
                        break;
                    case "Prototype-DevTeam":
                        SLEConnectionProperties.DbDataSource = System.Configuration.ConfigurationManager.AppSettings["PYDSLEDbDataSource"];
                        SLEConnectionProperties.DbInitialCatalog = System.Configuration.ConfigurationManager.AppSettings["PYDSLEDbInitialCatalog"];
                        SLEConnectionProperties.DbUserID = System.Configuration.ConfigurationManager.AppSettings["PYDSLEDbUserID"];
                        SLEConnectionProperties.DbPassword = System.Configuration.ConfigurationManager.AppSettings["PYDSLEDbPassword"];
                        SLEConnectionProperties.DbConnectTimeout = int.Parse(System.Configuration.ConfigurationManager.AppSettings["PYDSLEDbConnectTimeout"]);
                        SLEConnectionProperties.JDEWriteLinkSource = System.Configuration.ConfigurationManager.AppSettings["PYDSLEDbIntegratedSecurity"];
                        break;
                    default:
                        SLEConnectionProperties = null;
                        break;
                }
                return SLEConnectionProperties;
            }
            catch (Exception ex)
            {
                Errorlog("[DataTransformService].[SLEConnection].[GetConnectionDetails] Error Message:" + ex.Message + "  Error StackTrace" + ex.StackTrace);
            }
            return SLEConnectionProperties;
        }


        public InventoryExceptionService.DTService.ConnectionProperties GetSystemConnectionDetails()
        {
            WriteLog("System DB Connection Details Called");
            InventoryExceptionService.DTService.ConnectionProperties SLESystemConnectionProperties = new DTService.ConnectionProperties();
            try
            {
                SLESystemConnectionProperties.DbDataSource = System.Configuration.ConfigurationManager.AppSettings["SystemDbDataSource"];
                SLESystemConnectionProperties.DbInitialCatalog = System.Configuration.ConfigurationManager.AppSettings["SystemDbInitialCatalog"];
                SLESystemConnectionProperties.DbUserID = System.Configuration.ConfigurationManager.AppSettings["SystemDbUserID"];
                SLESystemConnectionProperties.DbPassword = System.Configuration.ConfigurationManager.AppSettings["SystemDbPassword"];
                SLESystemConnectionProperties.DbConnectTimeout = int.Parse(System.Configuration.ConfigurationManager.AppSettings["SystemDbConnectTimeout"]);
                SLESystemConnectionProperties.JDEWriteLinkSource = System.Configuration.ConfigurationManager.AppSettings["SystemDbIntegratedSecurity"];
                return SLESystemConnectionProperties;
            }
            catch (Exception ex)
            {
                Errorlog("[DataTransformService].[SLEConnection].[GetSystemConnectionDetails] Error Message:" + ex.Message + "  Error StackTrace" + ex.StackTrace);
            }
            return SLESystemConnectionProperties;
        }
        public List<InventoryExceptionService.DTService.ModuleSequence> GetJobSequenceList(InventoryExceptionService.DTService.ConnectionProperties model, int jodid)
        {
            List<InventoryExceptionService.DTService.ModuleSequence> moduleSeq = null;
            try
            {
                WriteLog("[DataTransformService].[SLEConnection] [Start:GetJobSequenceList]");
                SLEJDEInterfaceClient SLE = new SLEJDEInterfaceClient();
                moduleSeq = SLE.GetJobSequenceListDetail(model, jodid).ToList();
                WriteLog("[DataTransformService].[SLEConnection] [End:GetJobSequenceList]");
            }
            catch (Exception ex)
            {
                Errorlog("[DataTransformService].[SLEConnection].[GetJobSequenceList] Error Message:" + ex.Message + "  Error StackTrace" + ex.StackTrace);
            }
            return moduleSeq;
        }
        public EmailConfigDetails GetEmailConfig(InventoryExceptionService.DTService.ConnectionProperties model, int Emailid)
        {
            EmailConfigDetails EmailConfig = null;
            try
            {
                WriteLog("[DataTransformService].[SLEConnection] [Start:GetEmailConfig]");
                SLEJDEInterfaceClient SLE = new SLEJDEInterfaceClient();
                EmailConfig = SLE.GetEmailConfigDetail(model, Emailid);
                WriteLog("[DataTransformService].[SLEConnection] [End:GetEmailConfig]");
            }
            catch (Exception ex)
            {
                Errorlog("[DataTransformService].[SLEConnection].[GetJobSequenceList] Error Message:" + ex.Message + "  Error StackTrace" + ex.StackTrace);
            }
            return EmailConfig;
        }
        public List<string> GetEnvnameList(InventoryExceptionService.DTService.ConnectionProperties model)
        {
            List<string> envnamelist = new List<string>();

            try
            {
                WriteLog("[DataTransformService].[SLEConnection] [Start:GetEnvnameList]");
                SLEJDEInterfaceClient SLE = new SLEJDEInterfaceClient();

                envnamelist = SLE.GetEnvironmentList(model).ToList();
                WriteLog("[DataTransformService].[SLEConnection] [End:GetEnvnameList]");
            }
            catch (Exception ex)
            {
                Errorlog("[DataTransformService].[SLEConnection].[GetEnvnameList] Error Message:" + ex.Message + "  Error StackTrace" + ex.StackTrace);
            }
            return new List<string>();
        }


        public List<InventoryExceptionService.DTService.SettlementDetails> GetSettlementDetails(InventoryExceptionService.DTService.ConnectionProperties model)
        {
            List<InventoryExceptionService.DTService.SettlementDetails> LstSettlementDetails = null;
            try
            {
                WriteLog("[InventoryExceptionService].[SLEConnection] [Start:GetSettlementDetails]");
                SLEJDEInterfaceClient SLE = new SLEJDEInterfaceClient();
                LstSettlementDetails = SLE.GetRouteSettlementDetails(model).ToList();
            }
             catch (Exception ex)
            {
                Errorlog("[DataTransformService].[SLEConnection].[GetEnvnameList] Error Message:" + ex.Message + "  Error StackTrace" + ex.StackTrace);
            }
            return LstSettlementDetails;
        }


        public string ProcessInventoryException(InventoryExceptionService.DTService.ConnectionProperties model)
        {
            string status = null;
            try
            {
                WriteLog("[InventoryExceptionService].[SLEConnection] [Start:ProcessInventoryException]");
                WriteLog("Entry the  Process Inventory Exception....");
                List<InventoryExceptionService.DTService.SettlementDetails> LstSettlementDetails = GetSettlementDetails(model);
                foreach (InventoryExceptionService.DTService.SettlementDetails item in LstSettlementDetails)
                {
                    SLEJDEInterfaceClient SLE = new SLEJDEInterfaceClient();
                    status = SLE.InsertInventoryExceptionDetail(model, item);
                }
                WriteLog("[InventoryExceptionService].[SLEConnection] [End:ProcessInventoryException]");
                WriteLog("Process Inventory Exception End");

            }
            catch (Exception ex)
            {
                status = "\t InventoryException ERROR:\r\n \t\tError Message:\r\n \t" + ex.Message + "\r\n \t\t  Error StackTrace:\r\n \t" + ex.StackTrace;
                Errorlog("[DataTransformService].[SLEConnection].[ProcessInventoryException] Error Message:" + ex.Message + "  Error StackTrace" + ex.StackTrace);
            }
            return status;
        }

       

        public string GetRefSource(string environmentName)
        {
            string refSource = "";
            try
            {
                switch (environmentName)
                {
                    case "Prototype-User":
                        refSource = System.Configuration.ConfigurationManager.AppSettings["UserRefSource"];
                        break;
                    case "Prototype-DV":
                        refSource = System.Configuration.ConfigurationManager.AppSettings["DVRefSource"];
                        break;
                    case "Production-PD":
                        refSource = System.Configuration.ConfigurationManager.AppSettings["DVRefSource"];
                        break;
                    case "Prototype-DevTeam":
                        refSource = System.Configuration.ConfigurationManager.AppSettings["PYDRefSource"];
                        break;
                }
            }
            catch (Exception ex)
            {
                Errorlog("[DataTransformService].[SLEConnection].[GetRefSource] Error Message:" + ex.Message + "  Error StackTrace" + ex.StackTrace);
            }
            return refSource;
        }





        public List<InventoryExceptionService.DTService.ConditionalParameters> GetConditionalParameters(InventoryExceptionService.DTService.ConnectionProperties model, InventoryExceptionService.DTService.ConditionalParameters clsParameters)
        {
            List<InventoryExceptionService.DTService.ConditionalParameters> clsPara = null;
            try
            {
                WriteLog("[DataTransformService].[SLEConnection] [Start:GetConditionalParameters]");

                SLEJDEInterfaceClient SLE = new SLEJDEInterfaceClient();
                clsPara = SLE.GetScheduleList(model, clsParameters).ToList();
                WriteLog("[DataTransformService].[SLEConnection] [End:GetConditionalParameters]");

            }
            catch (Exception ex)
            {
                Errorlog("[DataTransformService].[SLEConnection].[GetConditionalParameters] Error Message:" + ex.Message + "  Error StackTrace" + ex.StackTrace);
            }
            return clsPara;
        }
        public string InsertUpdateSubModuleDetail(InventoryExceptionService.DTService.ConnectionProperties model, string JDEInterfaceID, string subModuleName, string moduleName, string envName, string status, InventoryExceptionService.DTService.ConditionalParameters clsParameters)
        {
            string statusret = "";
            try
            {
                WriteLog("[DataTransformService].[SLEConnection] [Start:InsertUpdateSubModuleDetail]");

                InventoryExceptionService.DTService.ModuleDetail Moduledetail = new ModuleDetail();
                Moduledetail.EnvironmentName = envName;
                Moduledetail.JDEInterfaceID = JDEInterfaceID;
                Moduledetail.ErrorStatus = string.Empty;
                SLEJDEInterfaceClient SLE = new SLEJDEInterfaceClient();
                statusret = SLE.InsertUpdateSubModuleDetail(model, Moduledetail);
                WriteLog("[DataTransformService].[SLEConnection] [End:InsertUpdateSubModuleDetail]");

            }
            catch (Exception ex)
            {
                Errorlog("[DataTransformService].[SLEConnection].[InsertUpdateSubModuleDetail] Error Message:" + ex.Message + "  Error StackTrace" + ex.StackTrace);
            }
            return statusret;

        }


        public void UpdateScheduleStatusDetail(InventoryExceptionService.DTService.ConnectionProperties model, string ServiceName, string JDEInterfaceID, string EnvironmentName, string status, string ScheduleStatus)
        {
            try
            {
                WriteLog("[DataTransformService].[SLEConnection] [Start:UpdateScheduleStatusDetail]");
                SLEJDEInterfaceClient SLE = new SLEJDEInterfaceClient();
                InventoryExceptionService.DTService.ModuleDetail moddetail = new ModuleDetail();
                moddetail.JDEInterfaceID = JDEInterfaceID;
                moddetail.ScheduleStatus = status;
                moddetail.ServiceName = ServiceName;
                moddetail.EnvironmentName = EnvironmentName;
                moddetail.ErrorStatus = status;
                SLE.UpdateScheduleDetail(model, moddetail);
                WriteLog("[DataTransformService].[SLEConnection] [End:UpdateScheduleStatusDetail]");
            }
            catch (Exception ex)
            {
                Errorlog("[DataTransformService].[SLEConnection].[UpdateScheduleStatusDetail] Error Message:" + ex.Message + "  Error StackTrace" + ex.StackTrace);
            }

        }
        public void UpdateModuleStatus(string subModuleName, string ModuleName, string JDEInterfaceID, string EnvironmentName, string status, InventoryExceptionService.DTService.ConnectionProperties model)
        {
            string statusret = "";
            try
            {
                WriteLog("[DataTransformService].[SLEConnection] [Start:UpdateModuleStatus]");

                InventoryExceptionService.DTService.ModuleDetail Moduledetail = new ModuleDetail();
                Moduledetail.EnvironmentName = EnvironmentName;
                Moduledetail.JDEInterfaceID = JDEInterfaceID;
                Moduledetail.ErrorStatus = status;
                Moduledetail.ScheduleStatus = status;
                Moduledetail.SUBModuleName = subModuleName;
                SLEJDEInterfaceClient SLE = new SLEJDEInterfaceClient();
                statusret = SLE.InsertUpdateSubModuleDetail(model, Moduledetail);
                WriteLog("[DataTransformService].[SLEConnection] [End:UpdateModuleStatus]");

            }
            catch (Exception ex)
            {
                Errorlog("[DataTransformService].[SLEConnection].[UpdateModuleStatus] Error Message:" + ex.Message + "  Error StackTrace" + ex.StackTrace);
            }

        }

        public void InsertServiceHistory(InventoryExceptionService.DTService.ConnectionProperties model, int JobID, int ScheduleID, string EnvironmentName, string status)
        {

            string statusret = "";
            try
            {
                WriteLog("[DataTransformService].[SLEConnection] [Start:InsertServiceHistory]");

                InventoryExceptionService.DTService.ServiceHistory objServiceHistory = new ServiceHistory();
                objServiceHistory.EnvironmentName = EnvironmentName;
                objServiceHistory.JobID = JobID;
                objServiceHistory.ScheduleID = ScheduleID;
                objServiceHistory.Status = status;
                SLEJDEInterfaceClient SLE = new SLEJDEInterfaceClient();
                statusret = SLE.InsertServiceHistory(model, objServiceHistory);
                WriteLog("[DataTransformService].[SLEConnection] [End:InsertServiceHistory]");

            }
            catch (Exception ex)
            {
                Errorlog("[DataTransformService].[SLEConnection].[InsertServiceHistory] Error Message:" + ex.Message + "  Error StackTrace" + ex.StackTrace);
            }

        }




        public void WriteLog(string message)
        {
            StreamWriter sm = null;
            try
            {
                sm = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile.txt", true);
                sm.WriteLine(DateTime.Now.ToString() + ": " + message);
                sm.Flush();
                sm.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public void Errorlog(string message)
        {
            StreamWriter sm = null;
            try
            {
                sm = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\ErrorLog.txt", true);
                sm.WriteLine(DateTime.Now.ToString() + ": " + message);
                sm.Flush();
                sm.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
