﻿using Models;
using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace SQLServerToJDE
{
    [ServiceContract]
    public interface ISLEJDEInterface : IDisposable
    {
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        string ProcessReplenishment(ConnectionProperties model);
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        string STOTProcessReplenishment(ConnectionProperties model);
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        string Update_STOTProcessReplenishment(ConnectionProperties model, string jdeRefSource);
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        string Update_STOTProcessReceipts(ConnectionProperties model, string jdeRefSource);
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        string ProcessSaleOrder(ConnectionProperties model);
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        string ProcessReceipts(ConnectionProperties model);
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        List<ConditionalParameters> GetScheduleList(ConnectionProperties model, ConditionalParameters Schedule);
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        string InsertUpdateSubModuleDetail(ConnectionProperties model, ModuleDetail Moduledetail);
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        string UpdateScheduleDetail(ConnectionProperties model, ModuleDetail UpdateModule);
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        List<ModuleSequence> GetJobSequenceListDetail(ConnectionProperties model, int jodid);
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        EmailConfigDetails GetEmailConfigDetail(ConnectionProperties model, int Emailid);
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        string InsertServiceHistory(ConnectionProperties model, ServiceHistory ServiceHistory);
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        int UpdateJDEStatus(ConnectionProperties model);
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        int UpdateVarianceStatus(ConnectionProperties model);
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        List<string> GetEnvironmentList(ConnectionProperties model);
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        string InsertMetricDetails(ConnectionProperties model);
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        string InsertInventoryDetail(ConnectionProperties model);
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        string InsertDeliveryCycles(ConnectionProperties model);
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        string InsertInventoryExceptionDetail(ConnectionProperties model,SettlementDetails settlementdetail);
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        List<SettlementDetails> GetRouteSettlementDetails(ConnectionProperties model);
    }
}
