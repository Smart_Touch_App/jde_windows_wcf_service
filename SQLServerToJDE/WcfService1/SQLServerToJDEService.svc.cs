﻿using BAL;
using Models;
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Text;

namespace SQLServerToJDE
{
    //[ServiceBehavior(TransactionIsolationLevel = System.Transactions.IsolationLevel.Serializable, TransactionTimeout = "00:10:00")]
    public class SLEJDEInterface : ISLEJDEInterface
    {
        UnitOfWork UnitOfWork;
        public SLEJDEInterface()
        {
            UnitOfWork = new UnitOfWork();
        }

        //[OperationBehavior(TransactionScopeRequired = true)]
        public string ProcessReplenishment(ConnectionProperties model)
        {
            try
            {
                return UnitOfWork.SampleRepository.ProcessReplenishment(model);
            }
            catch (Exception ex)
            {
                StringBuilder errorMessage = new StringBuilder();
                errorMessage.Append("Message: ");
                errorMessage.Append(ex.Message);
                errorMessage.AppendLine();
                errorMessage.Append(",StackTrace: ");
                errorMessage.Append(ex.StackTrace);
                if (ex.InnerException != null)
                {
                    errorMessage.AppendLine();
                    errorMessage.Append(",InnerExceptionMessage: ");
                    errorMessage.Append(ex.InnerException.Message);
                    errorMessage.AppendLine();
                    errorMessage.Append(",InnerExceptionStackTrace: ");
                    errorMessage.Append(ex.InnerException.StackTrace);
                }
                throw new FaultException(new FaultReason(errorMessage.ToString()));
            }
        }

        public string STOTProcessReplenishment(ConnectionProperties model)
        {
            try
            {
                return UnitOfWork.SampleRepository.STOTProcessReplenishment(model);
            }
            catch (Exception ex)
            {
                StringBuilder errorMessage = new StringBuilder();
                errorMessage.Append("Message: ");
                errorMessage.Append(ex.Message);
                errorMessage.AppendLine();
                errorMessage.Append(",StackTrace: ");
                errorMessage.Append(ex.StackTrace);
                if (ex.InnerException != null)
                {
                    errorMessage.AppendLine();
                    errorMessage.Append(",InnerExceptionMessage: ");
                    errorMessage.Append(ex.InnerException.Message);
                    errorMessage.AppendLine();
                    errorMessage.Append(",InnerExceptionStackTrace: ");
                    errorMessage.Append(ex.InnerException.StackTrace);
                }
                throw new FaultException(new FaultReason(errorMessage.ToString()));
            }
        }

        public string Update_STOTProcessReplenishment(ConnectionProperties model, string jdeRefSource)
        {
            try
            {
                return UnitOfWork.SampleRepository.Update_STOTProcessReplenishment(model, jdeRefSource);
            }
            catch (Exception ex)
            {
                StringBuilder errorMessage = new StringBuilder();
                errorMessage.Append("Message: ");
                errorMessage.Append(ex.Message);
                errorMessage.AppendLine();
                errorMessage.Append(",StackTrace: ");
                errorMessage.Append(ex.StackTrace);
                if (ex.InnerException != null)
                {
                    errorMessage.AppendLine();
                    errorMessage.Append(",InnerExceptionMessage: ");
                    errorMessage.Append(ex.InnerException.Message);
                    errorMessage.AppendLine();
                    errorMessage.Append(",InnerExceptionStackTrace: ");
                    errorMessage.Append(ex.InnerException.StackTrace);
                }
                throw new FaultException(new FaultReason(errorMessage.ToString()));
            }
        }
        public string Update_STOTProcessReceipts(ConnectionProperties model, string jdeRefSource)
        {
            try
            {
                return UnitOfWork.SampleRepository.InsertAndUpdate_STOTReceipts(model, jdeRefSource);
            }
            catch (Exception ex)
            {
                StringBuilder errorMessage = new StringBuilder();
                errorMessage.Append("Message: ");
                errorMessage.Append(ex.Message);
                errorMessage.AppendLine();
                errorMessage.Append(",StackTrace: ");
                errorMessage.Append(ex.StackTrace);
                if (ex.InnerException != null)
                {
                    errorMessage.AppendLine();
                    errorMessage.Append(",InnerExceptionMessage: ");
                    errorMessage.Append(ex.InnerException.Message);
                    errorMessage.AppendLine();
                    errorMessage.Append(",InnerExceptionStackTrace: ");
                    errorMessage.Append(ex.InnerException.StackTrace);
                }
                throw new FaultException(new FaultReason(errorMessage.ToString()));
            }
        }



        public string InsertInventoryDetail(ConnectionProperties model)
        {
            try
            {
                return UnitOfWork.InsertInventoryDetail.InventoryDetail(model);
            }
            catch (Exception ex)
            {
                StringBuilder errorMessage = new StringBuilder();
                errorMessage.Append("Message: ");
                errorMessage.Append(ex.Message);
                errorMessage.AppendLine();
                errorMessage.Append(",StackTrace: ");
                errorMessage.Append(ex.StackTrace);
                if (ex.InnerException != null)
                {
                    errorMessage.AppendLine();
                    errorMessage.Append(",InnerExceptionMessage: ");
                    errorMessage.Append(ex.InnerException.Message);
                    errorMessage.AppendLine();
                    errorMessage.Append(",InnerExceptionStackTrace: ");
                    errorMessage.Append(ex.InnerException.StackTrace);
                }
                throw new FaultException(new FaultReason(errorMessage.ToString()));
            }
        }
        public string InsertInventoryExceptionDetail(ConnectionProperties model,SettlementDetails settlementdetail)
        {
            try
            {
                return UnitOfWork.InsertInventoryDetail.InsertInventoryExceptionDetail(model,settlementdetail);
            }
            catch (Exception ex)
            {
                StringBuilder errorMessage = new StringBuilder();
                errorMessage.Append("Message: ");
                errorMessage.Append(ex.Message);
                errorMessage.AppendLine();
                errorMessage.Append(",StackTrace: ");
                errorMessage.Append(ex.StackTrace);
                if (ex.InnerException != null)
                {
                    errorMessage.AppendLine();
                    errorMessage.Append(",InnerExceptionMessage: ");
                    errorMessage.Append(ex.InnerException.Message);
                    errorMessage.AppendLine();
                    errorMessage.Append(",InnerExceptionStackTrace: ");
                    errorMessage.Append(ex.InnerException.StackTrace);
                }
                throw new FaultException(new FaultReason(errorMessage.ToString()));
            }
        }

        public List<SettlementDetails> GetRouteSettlementDetails(ConnectionProperties model)
        {
            try
            {
                return UnitOfWork.InsertInventoryDetail.GetRouteSettlementDetails(model);
            }
            catch (Exception ex)
            {
                StringBuilder errorMessage = new StringBuilder();
                errorMessage.Append("Message: ");
                errorMessage.Append(ex.Message);
                errorMessage.AppendLine();
                errorMessage.Append(",StackTrace: ");
                errorMessage.Append(ex.StackTrace);
                if (ex.InnerException != null)
                {
                    errorMessage.AppendLine();
                    errorMessage.Append(",InnerExceptionMessage: ");
                    errorMessage.Append(ex.InnerException.Message);
                    errorMessage.AppendLine();
                    errorMessage.Append(",InnerExceptionStackTrace: ");
                    errorMessage.Append(ex.InnerException.StackTrace);
                }
                throw new FaultException(new FaultReason(errorMessage.ToString()));
            }
        }

         //[OperationBehavior(TransactionScopeRequired = true)]
        public string ProcessReceipts(ConnectionProperties model)
        {
            try
            {
                return UnitOfWork.ReceiptRepository.ProcessReceipts(model);
            }
            catch (Exception ex)
            {
                StringBuilder errorMessage = new StringBuilder();
                errorMessage.Append("Message: ");
                errorMessage.Append(ex.Message);
                errorMessage.AppendLine();
                errorMessage.Append(",StackTrace: ");
                errorMessage.Append(ex.StackTrace);
                if (ex.InnerException != null)
                {
                    errorMessage.AppendLine();
                    errorMessage.Append(",InnerExceptionMessage: ");
                    errorMessage.Append(ex.InnerException.Message);
                    errorMessage.AppendLine();
                    errorMessage.Append(",InnerExceptionStackTrace: ");
                    errorMessage.Append(ex.InnerException.StackTrace);
                }
                throw new FaultException(new FaultReason(errorMessage.ToString()));
            }
        }
        //[OperationBehavior(TransactionScopeRequired = true)]
        public List<string> GetEnvironmentList(ConnectionProperties model)
        {
            try
            {
                return UnitOfWork.GetScheduleList.GetEnvironmentList(model);
            }
            catch (Exception ex)
            {
                StringBuilder errorMessage = new StringBuilder();
                errorMessage.Append("Message: ");
                errorMessage.Append(ex.Message);
                errorMessage.AppendLine();
                errorMessage.Append(",StackTrace: ");
                errorMessage.Append(ex.StackTrace);
                if (ex.InnerException != null)
                {
                    errorMessage.AppendLine();
                    errorMessage.Append(",InnerExceptionMessage: ");
                    errorMessage.Append(ex.InnerException.Message);
                    errorMessage.AppendLine();
                    errorMessage.Append(",InnerExceptionStackTrace: ");
                    errorMessage.Append(ex.InnerException.StackTrace);
                }
                throw new FaultException(new FaultReason(errorMessage.ToString()));
            }
        }
        //[OperationBehavior(TransactionScopeRequired = true)]
        public string ProcessSaleOrder(ConnectionProperties model)
        {
            try
            {
                return UnitOfWork.SaleOrderRepository.ProcessSaleOrder(model);
            }
            catch (Exception ex)
            {
                StringBuilder errorMessage = new StringBuilder();
                errorMessage.Append("Message: ");
                errorMessage.Append(ex.Message);
                errorMessage.AppendLine();
                errorMessage.Append(",StackTrace: ");
                errorMessage.Append(ex.StackTrace);
                if (ex.InnerException != null)
                {
                    errorMessage.AppendLine();
                    errorMessage.Append(",InnerExceptionMessage: ");
                    errorMessage.Append(ex.InnerException.Message);
                    errorMessage.AppendLine();
                    errorMessage.Append(",InnerExceptionStackTrace: ");
                    errorMessage.Append(ex.InnerException.StackTrace);
                }
                throw new FaultException(new FaultReason(errorMessage.ToString()));
            }
        }

        //[OperationBehavior(TransactionScopeRequired = true)]
        public List<ConditionalParameters> GetScheduleList(ConnectionProperties model, ConditionalParameters Schedule)
        {
            try
            {
                return UnitOfWork.GetScheduleList.GetScheduleList(model, Schedule);
            }
            catch (Exception ex)
            {
                StringBuilder errorMessage = new StringBuilder();
                errorMessage.Append("Message: ");
                errorMessage.Append(ex.Message);
                errorMessage.AppendLine();
                errorMessage.Append(",StackTrace: ");
                errorMessage.Append(ex.StackTrace);
                if (ex.InnerException != null)
                {
                    errorMessage.AppendLine();
                    errorMessage.Append(",InnerExceptionMessage: ");
                    errorMessage.Append(ex.InnerException.Message);
                    errorMessage.AppendLine();
                    errorMessage.Append(",InnerExceptionStackTrace: ");
                    errorMessage.Append(ex.InnerException.StackTrace);
                }
                throw new FaultException(new FaultReason(errorMessage.ToString()));
            }
        }
       // [OperationBehavior(TransactionScopeRequired = true)]
        public List<ModuleSequence> GetJobSequenceListDetail(ConnectionProperties model, int jodid)
        {
            try
            {
                return UnitOfWork.JobSequenceList.GetSequenceList(model, jodid);
            }
            catch (Exception ex)
            {
                StringBuilder errorMessage = new StringBuilder();
                errorMessage.Append("Message: ");
                errorMessage.Append(ex.Message);
                errorMessage.AppendLine();
                errorMessage.Append(",StackTrace: ");
                errorMessage.Append(ex.StackTrace);
                if (ex.InnerException != null)
                {
                    errorMessage.AppendLine();
                    errorMessage.Append(",InnerExceptionMessage: ");
                    errorMessage.Append(ex.InnerException.Message);
                    errorMessage.AppendLine();
                    errorMessage.Append(",InnerExceptionStackTrace: ");
                    errorMessage.Append(ex.InnerException.StackTrace);
                }
                throw new FaultException(new FaultReason(errorMessage.ToString()));
            }

        }
        //[OperationBehavior(TransactionScopeRequired = true)]
        public EmailConfigDetails GetEmailConfigDetail(ConnectionProperties model, int Emailid)
        {
            try
            {
                return UnitOfWork.EmailConfigDetail.GetEmailConfig(model, Emailid);
            }
            catch (Exception ex)
            {
                StringBuilder errorMessage = new StringBuilder();
                errorMessage.Append("Message: ");
                errorMessage.Append(ex.Message);
                errorMessage.AppendLine();
                errorMessage.Append(",StackTrace: ");
                errorMessage.Append(ex.StackTrace);
                if (ex.InnerException != null)
                {
                    errorMessage.AppendLine();
                    errorMessage.Append(",InnerExceptionMessage: ");
                    errorMessage.Append(ex.InnerException.Message);
                    errorMessage.AppendLine();
                    errorMessage.Append(",InnerExceptionStackTrace: ");
                    errorMessage.Append(ex.InnerException.StackTrace);
                }
                throw new FaultException(new FaultReason(errorMessage.ToString()));
            }


        }
        //[OperationBehavior(TransactionScopeRequired = true)]
        public string InsertUpdateSubModuleDetail(ConnectionProperties model, ModuleDetail Moduledetail)
        {
            try
            {
                return UnitOfWork.GetSubModuleDetail.AddUpdateSubModuleDetail(model,Moduledetail);
            }
            catch (Exception ex)
            {
                StringBuilder errorMessage = new StringBuilder();
                errorMessage.Append("Message: ");
                errorMessage.Append(ex.Message);
                errorMessage.AppendLine();
                errorMessage.Append(",StackTrace: ");
                errorMessage.Append(ex.StackTrace);
                if (ex.InnerException != null)
                {
                    errorMessage.AppendLine();
                    errorMessage.Append(",InnerExceptionMessage: ");
                    errorMessage.Append(ex.InnerException.Message);
                    errorMessage.AppendLine();
                    errorMessage.Append(",InnerExceptionStackTrace: ");
                    errorMessage.Append(ex.InnerException.StackTrace);
                }
                throw new FaultException(new FaultReason(errorMessage.ToString()));
            }
        }
        //[OperationBehavior(TransactionScopeRequired = true)]
        public string UpdateScheduleDetail(ConnectionProperties model, ModuleDetail UpdateModule)
        {
            try
            {
                return UnitOfWork.UpdateScheduleDetail.UpdateScheduleDetail(model, UpdateModule);
            }
            catch (Exception ex)
            {
                StringBuilder errorMessage = new StringBuilder();
                errorMessage.Append("Message: ");
                errorMessage.Append(ex.Message);
                errorMessage.AppendLine();
                errorMessage.Append(",StackTrace: ");
                errorMessage.Append(ex.StackTrace);
                if (ex.InnerException != null)
                {
                    errorMessage.AppendLine();
                    errorMessage.Append(",InnerExceptionMessage: ");
                    errorMessage.Append(ex.InnerException.Message);
                    errorMessage.AppendLine();
                    errorMessage.Append(",InnerExceptionStackTrace: ");
                    errorMessage.Append(ex.InnerException.StackTrace);
                }
                throw new FaultException(new FaultReason(errorMessage.ToString()));
            }
        }
        //[OperationBehavior(TransactionScopeRequired = true)]
        public string InsertServiceHistory(ConnectionProperties model, ServiceHistory ServiceHistory)
        {
            try
            {
                return UnitOfWork.ServiceHistory.InsertServiceHistory(model, ServiceHistory);
            }
            catch (Exception ex)
            {
                StringBuilder errorMessage = new StringBuilder();
                errorMessage.Append("Message: ");
                errorMessage.Append(ex.Message);
                errorMessage.AppendLine();
                errorMessage.Append(",StackTrace: ");
                errorMessage.Append(ex.StackTrace);
                if (ex.InnerException != null)
                {
                    errorMessage.AppendLine();
                    errorMessage.Append(",InnerExceptionMessage: ");
                    errorMessage.Append(ex.InnerException.Message);
                    errorMessage.AppendLine();
                    errorMessage.Append(",InnerExceptionStackTrace: ");
                    errorMessage.Append(ex.InnerException.StackTrace);
                }
                throw new FaultException(new FaultReason(errorMessage.ToString()));
            }
        }
        //[OperationBehavior(TransactionScopeRequired = true)]
        public int UpdateJDEStatus(ConnectionProperties model)
        {
            try
            {
                UnitOfWork.UpdateJDEStatus.UpdateJDEStatusDetail(model);
                return 0;
            }
            catch (Exception ex)
            {
                StringBuilder errorMessage = new StringBuilder();
                errorMessage.Append("Message: ");
                errorMessage.Append(ex.Message);
                errorMessage.AppendLine();
                errorMessage.Append(",StackTrace: ");
                errorMessage.Append(ex.StackTrace);
                if (ex.InnerException != null)
                {
                    errorMessage.AppendLine();
                    errorMessage.Append(",InnerExceptionMessage: ");
                    errorMessage.Append(ex.InnerException.Message);
                    errorMessage.AppendLine();
                    errorMessage.Append(",InnerExceptionStackTrace: ");
                    errorMessage.Append(ex.InnerException.StackTrace);
                }
                throw new FaultException(new FaultReason(errorMessage.ToString()));
            }
        }
       
        

        public int UpdateVarianceStatus(ConnectionProperties model)
        {
            try
            {
                UnitOfWork.UpdateJDEStatus.UpdateVarianceStatusDetail(model);
                return 0;
            }
            catch (Exception ex)
            {
                StringBuilder errorMessage = new StringBuilder();
                errorMessage.Append("Message: ");
                errorMessage.Append(ex.Message);
                errorMessage.AppendLine();
                errorMessage.Append(",StackTrace: ");
                errorMessage.Append(ex.StackTrace);
                if (ex.InnerException != null)
                {
                    errorMessage.AppendLine();
                    errorMessage.Append(",InnerExceptionMessage: ");
                    errorMessage.Append(ex.InnerException.Message);
                    errorMessage.AppendLine();
                    errorMessage.Append(",InnerExceptionStackTrace: ");
                    errorMessage.Append(ex.InnerException.StackTrace);
                }
                throw new FaultException(new FaultReason(errorMessage.ToString()));
            }
        }

        public string InsertMetricDetails(ConnectionProperties model)
        {
            try
            {
                return UnitOfWork.InsertMetricsData.InsertMetricData(model);
            }
            catch (Exception ex)
            {
                StringBuilder errorMessage = new StringBuilder();
                errorMessage.Append("Message: ");
                errorMessage.Append(ex.Message);
                errorMessage.AppendLine();
                errorMessage.Append(",StackTrace: ");
                errorMessage.Append(ex.StackTrace);
                if (ex.InnerException != null)
                {
                    errorMessage.AppendLine();
                    errorMessage.Append(",InnerExceptionMessage: ");
                    errorMessage.Append(ex.InnerException.Message);
                    errorMessage.AppendLine();
                    errorMessage.Append(",InnerExceptionStackTrace: ");
                    errorMessage.Append(ex.InnerException.StackTrace);
                }
                throw new FaultException(new FaultReason(errorMessage.ToString()));
            }
        }
        public string InsertDeliveryCycles(ConnectionProperties model)
        {
            try
            {
                return UnitOfWork.InsertMetricsData.InsertDeliveryCycles(model);
            }
            catch (Exception ex)
            {
                StringBuilder errorMessage = new StringBuilder();
                errorMessage.Append("Message: ");
                errorMessage.Append(ex.Message);
                errorMessage.AppendLine();
                errorMessage.Append(",StackTrace: ");
                errorMessage.Append(ex.StackTrace);
                if (ex.InnerException != null)
                {
                    errorMessage.AppendLine();
                    errorMessage.Append(",InnerExceptionMessage: ");
                    errorMessage.Append(ex.InnerException.Message);
                    errorMessage.AppendLine();
                    errorMessage.Append(",InnerExceptionStackTrace: ");
                    errorMessage.Append(ex.InnerException.StackTrace);
                }
                throw new FaultException(new FaultReason(errorMessage.ToString()));
            }
        }

       
        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
                if (UnitOfWork != null)
                    UnitOfWork.Dispose();
            }
            disposed = true;
        }

        ~SLEJDEInterface()
        {
            Dispose(false);
        }
    }
}
