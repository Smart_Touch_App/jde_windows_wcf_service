﻿using BAL.Repository;
using System;

namespace BAL
{
    public class UnitOfWork : IUnitOfWork
    {
        IReplenishmentRepository sampleRepository;
        ISaleOrderRepository saleorderrepository;
        IGetScheduleDetails GetscheduleList;
        ISubModuleDetails Getsubmoduledetail;
        IUpdateSchedule UpadatescheduleDetail;
        IReceiptsRepository Receiptrepository;
        IEmailConfig Emailconfig;
        IJobSequenceDetails Jobsequencedetails;
        IServiceHistoryDetail servicehistory;
        IUpdateJDEStatus JDEStatus;
        IMetricsData metricsdata;
        IInventoryDetail insertinventorydetail;

        public IInventoryDetail InsertInventoryDetail
        {
            get
            {
                if (insertinventorydetail == null)
                    insertinventorydetail = new InventoryRepository();
                return insertinventorydetail;
            }
        }

        public IReplenishmentRepository SampleRepository
        {
            get
            {
                if (sampleRepository == null)
                    sampleRepository = new ReplenishmentRepository();
                return sampleRepository;
            }
        }
        public IMetricsData InsertMetricsData
        {
            get
            {
                if (metricsdata == null)
                    metricsdata = new MetricsData();
                return metricsdata;
            }
        }
        public IUpdateJDEStatus UpdateJDEStatus
        {
            get
            {
                if (JDEStatus == null)
                    JDEStatus = new UpdateJDEStatus();
                return JDEStatus;
            }
        }

        public IEmailConfig EmailConfigDetail
        {
            get
            {
                if (Emailconfig == null)
                    Emailconfig = new EmailConfig();
                return Emailconfig;
            }
        }
        public IJobSequenceDetails JobSequenceList
        {
            get
            {
                if (Jobsequencedetails == null)
                    Jobsequencedetails = new JobSequenceDetails();
                return Jobsequencedetails;
            }
        }
        public IReceiptsRepository ReceiptRepository
        {
            get
            {
                if (Receiptrepository == null)
                    Receiptrepository = new ReceiptsRepository();
                return Receiptrepository;
            }
        }
        public ISaleOrderRepository SaleOrderRepository
        {
            get
            {
                if (saleorderrepository == null)
                    saleorderrepository = new SaleOrderRepository();
                return saleorderrepository;
            }
        }
        public IServiceHistoryDetail ServiceHistory
        {
            get
            {
                if (servicehistory == null)
                    servicehistory = new ServiceHistoryDetail();
                return servicehistory;
            }
        }


        public IGetScheduleDetails GetScheduleList
        {
            get
            {
                if (GetscheduleList == null)
                    GetscheduleList = new GetScheduleDetails();
                return GetscheduleList;
            }
        }
        public ISubModuleDetails GetSubModuleDetail
        {
            get
            {
                if (Getsubmoduledetail == null)
                    Getsubmoduledetail = new SubModuleDetails();
                return Getsubmoduledetail;
            }
        }

        public IUpdateSchedule UpdateScheduleDetail
        {
            get
            {
                if (UpadatescheduleDetail == null)
                    UpadatescheduleDetail = new UpdateSchedule();
                return UpadatescheduleDetail;
            }
        }


        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
                if (sampleRepository != null)
                    sampleRepository.Dispose();
            }
            disposed = true;
        }

        ~UnitOfWork()
        {
            Dispose(false);
        }
    }
}
