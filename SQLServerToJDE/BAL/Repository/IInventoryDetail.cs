﻿using Models;
using System;
using System.Collections.Generic;

namespace BAL.Repository
{
    public interface IInventoryDetail : IDisposable
    {
        string InventoryDetail(ConnectionProperties model);
        string InsertInventoryExceptionDetail(ConnectionProperties model, SettlementDetails settlementdetail);
        List<SettlementDetails> GetRouteSettlementDetails(ConnectionProperties model);
    }
}
