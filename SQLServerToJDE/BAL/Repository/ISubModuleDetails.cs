﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Interface created by Marimuthu
namespace BAL.Repository
{
    public interface ISubModuleDetails
    {
        string AddUpdateSubModuleDetail(ConnectionProperties model, ModuleDetail SubModule);
    }
}
