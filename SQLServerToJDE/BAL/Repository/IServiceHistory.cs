﻿using Models;

namespace BAL.Repository
{
    public interface IServiceHistoryDetail
    {
        string InsertServiceHistory(ConnectionProperties model, ServiceHistory Servicedetails);
    }
}
