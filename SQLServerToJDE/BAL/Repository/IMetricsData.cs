﻿using Models;

namespace BAL.Repository
{
    public interface IMetricsData
    {
        string InsertMetricData(ConnectionProperties model);
        string InsertDeliveryCycles(ConnectionProperties model);
    }
}
