﻿using Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace BAL.Repository
{
    public class InventoryRepository : IInventoryDetail
    {
        private SqlConnectionStringBuilder ConnBuilder;
        public InventoryRepository()
        {
            ConnBuilder = new SqlConnectionStringBuilder();
        }

        public string InventoryDetail(ConnectionProperties model)
        {
            if (model != null)
            {
                try
                {
                    ConnBuilder.DataSource = model.DbDataSource;
                    ConnBuilder.InitialCatalog = model.DbInitialCatalog;
                    if (model.DbIntegratedSecurity == false)
                    {
                        ConnBuilder.UserID = model.DbUserID;
                        ConnBuilder.Password = model.DbPassword;
                    }
                    //ConnBuilder.ConnectTimeout = Int32.MaxValue;
                    ConnBuilder.IntegratedSecurity = model.DbIntegratedSecurity;

                    using (SqlConnection Conn = new SqlConnection(ConnBuilder.ConnectionString))
                    {
                        using (SqlCommand comm = new SqlCommand("[BUSDTA].[SP_InsertInventoryDetail]", Conn))
                        {
                            comm.CommandType = CommandType.StoredProcedure;
                            Conn.Open();
                            comm.ExecuteNonQuery();
                        }
                    }
                }
                catch
                {
                    throw;
                }
            }
            return "Completed";
        }
        public string InsertInventoryExceptionDetail(ConnectionProperties model, SettlementDetails settlementdetail)
        {

            if (model != null)
            {
                try
                {
                    WriteLog("[InventoryRepository][InsertInventoryExceptionDetail]");
                  
                    ConnBuilder.DataSource = model.DbDataSource;
                    ConnBuilder.InitialCatalog = model.DbInitialCatalog;
                    if (model.DbIntegratedSecurity == false)
                    {
                        ConnBuilder.UserID = model.DbUserID;
                        ConnBuilder.Password = model.DbPassword;
                    }
                    //ConnBuilder.ConnectTimeout = Int32.MaxValue;
                    ConnBuilder.IntegratedSecurity = model.DbIntegratedSecurity;

                    using (SqlConnection Conn = new SqlConnection(ConnBuilder.ConnectionString))
                    {
                        using (SqlCommand comm = new SqlCommand("[BUSDTA].[Sp_GetExceptiontable] '" + settlementdetail.Fromdate.ToString() + "','" + settlementdetail.Todate.ToString() + "','" + settlementdetail.Routeid + "'", Conn))
                        {
                            WriteLog("" + comm.CommandText + "");
                            comm.CommandType = CommandType.Text;
                            //comm.Parameters.AddWithValue("@FromDate", settlementdetail.Fromdate);
                            //comm.Parameters.AddWithValue("@ToDate", settlementdetail.Todate);
                            //comm.Parameters.AddWithValue("@routeList", settlementdetail.Routeid);
                            Conn.Open();
                            comm.ExecuteNonQuery();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Errorlog("[InventoryRepository][InsertInventoryExceptionDetail] Error Message:" + ex.Message + "  Error StackTrace" + ex.StackTrace);
                    throw;
                }
            }
            return "Completed";
        }

        public List<SettlementDetails> GetRouteSettlementDetails(ConnectionProperties model)
        {
            SqlDataReader sqlDatardrHeader = null;
            List<SettlementDetails> SettlementDetailslist = new List<SettlementDetails>();
            if (model != null)
            {
                try
                {
                    ConnBuilder.DataSource = model.DbDataSource;
                    ConnBuilder.InitialCatalog = model.DbInitialCatalog;
                    if (model.DbIntegratedSecurity == false)
                    {
                        ConnBuilder.UserID = model.DbUserID;
                        ConnBuilder.Password = model.DbPassword;
                    }
                    //ConnBuilder.ConnectTimeout = Int32.MaxValue;
                    ConnBuilder.IntegratedSecurity = model.DbIntegratedSecurity;

                    using (SqlConnection Conn = new SqlConnection(ConnBuilder.ConnectionString))
                    {
                        using (SqlCommand comm = new SqlCommand("BUSDTA.SP_GetSettlementDetailsForException", Conn))
                        {
                            comm.CommandType = CommandType.StoredProcedure;
                            Conn.Open();
                            sqlDatardrHeader = comm.ExecuteReader();
                            while (sqlDatardrHeader.Read())
                            {
                                SettlementDetails Settlement = new SettlementDetails();

                                Settlement.Routeid = sqlDatardrHeader["RouteId"].ToString();
                                Settlement.Fromdate = sqlDatardrHeader["FromDate"].ToString();
                                Settlement.Todate = sqlDatardrHeader["ToDate"].ToString();

                                SettlementDetailslist.Add(Settlement);
                            }
                        }
                        Conn.Close();
                        Conn.Dispose();
                    }
                }
                catch
                {
                    throw;
                }
            }
            return SettlementDetailslist;
        }


        public void WriteLog(string message)
        {
            StreamWriter sm = null;
            try
            {
                sm = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile.txt", true);
                sm.WriteLine(DateTime.Now.ToString() + ": " + message);
                sm.Flush();
                sm.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Errorlog(string message)
        {
            StreamWriter sm = null;
            try
            {
                sm = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\ErrorLog.txt", true);
                sm.WriteLine(DateTime.Now.ToString() + ": " + message);
                sm.Flush();
                sm.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }

        ~InventoryRepository()
        {
            Dispose(false);
        }
    }
}
