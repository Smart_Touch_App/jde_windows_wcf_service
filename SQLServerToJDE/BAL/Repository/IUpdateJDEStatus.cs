﻿using Models;

namespace BAL.Repository
{
    public interface IUpdateJDEStatus
    {
        int UpdateJDEStatusDetail(ConnectionProperties model);
        int UpdateVarianceStatusDetail(ConnectionProperties model);
    }
}
