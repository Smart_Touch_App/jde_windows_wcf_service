﻿using Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.Repository
{
    public class SaleOrderRepository : ISaleOrderRepository
    {
        private SqlConnectionStringBuilder ConnBuilder;

        public SaleOrderRepository()
        {
            ConnBuilder = new SqlConnectionStringBuilder();
        }
        //Method added by Vignesh.S
        public string ProcessSaleOrder(ConnectionProperties model)
        {
            if (model != null)
            {
                try
                {
                    ConnBuilder.DataSource = model.DbDataSource;
                    ConnBuilder.InitialCatalog = model.DbInitialCatalog;
                    if (model.DbIntegratedSecurity == false)
                    {
                        ConnBuilder.UserID = model.DbUserID;
                        ConnBuilder.Password = model.DbPassword;
                    }
                    ConnBuilder.ConnectTimeout = Int32.MaxValue;
                    ConnBuilder.IntegratedSecurity = model.DbIntegratedSecurity;

                    using (SqlConnection Conn = new SqlConnection(ConnBuilder.ConnectionString))
                    {
                        using (SqlCommand comm = new SqlCommand("BUSDTA.SP_InsertSaleOrderDetail", Conn))
                        {
                            comm.CommandType = CommandType.StoredProcedure;
                            comm.Parameters.AddWithValue("@jdeLinkSource", model.JDEWriteLinkSource);
                            //comm.CommandTimeout = 100000;
                            Conn.Open();
                            int result = comm.ExecuteNonQuery();
                        }
                        //Conn.Close();
                        //Conn.Dispose();
                    }
                }
                catch
                {
                    throw;
                }
            }
            return "Completed";
        }

        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }

        ~SaleOrderRepository()
        {
            Dispose(false);
        }
    }
}
