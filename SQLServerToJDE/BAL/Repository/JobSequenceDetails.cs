﻿using Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace BAL.Repository
{
    public class JobSequenceDetails : IJobSequenceDetails
    {
        private SqlConnectionStringBuilder ConnBuilder;

        public JobSequenceDetails()
        {
            ConnBuilder = new SqlConnectionStringBuilder();
        }
        //Method added by Vignesh.S
        public List<ModuleSequence> GetSequenceList(ConnectionProperties model, int jodid)
        {

            List<ModuleSequence> SequenceList = new List<ModuleSequence>();
            SqlDataReader sqlDatardrHeader = null;
            if (model != null)
            {
                try
                {
                    ConnBuilder.DataSource = model.DbDataSource;
                    ConnBuilder.InitialCatalog = model.DbInitialCatalog;
                    if (model.DbIntegratedSecurity == false)
                    {
                        ConnBuilder.UserID = model.DbUserID;
                        ConnBuilder.Password = model.DbPassword;
                    }
                    ConnBuilder.ConnectTimeout = model.DbConnectTimeout;
                    ConnBuilder.IntegratedSecurity = model.DbIntegratedSecurity;

                    using (SqlConnection Conn = new SqlConnection(ConnBuilder.ConnectionString))
                    {
                        using (SqlCommand comm = new SqlCommand("BUSDTA.SP_GetJobSequenceDetails", Conn))
                        {
                            comm.CommandType = CommandType.StoredProcedure;
                            comm.Parameters.AddWithValue("@JobID", jodid);
                            Conn.Open();
                            sqlDatardrHeader= comm.ExecuteReader();
                           while (sqlDatardrHeader.Read())
                           {
                               ModuleSequence Sequence = new ModuleSequence();
                               Sequence.JobID = int.Parse(sqlDatardrHeader["JobID"].ToString());
                               Sequence.JobName = sqlDatardrHeader["JobName"].ToString();
                               Sequence.SequenceNo = int.Parse(sqlDatardrHeader["SequenceNo"].ToString());
                               Sequence.ModuleName = sqlDatardrHeader["ModuleName"].ToString();
                               Sequence.Dependency = sqlDatardrHeader["Dependency"].ToString() == "true" ? true : false;
                               Sequence.EmailNotification = sqlDatardrHeader["EmailNotification"].ToString() == "true" ? true : false;
                               Sequence.EmailConfigID = int.Parse(sqlDatardrHeader["EmailConfigID"].ToString());
                               SequenceList.Add(Sequence);
                           }
                        }
                        Conn.Close();
                        Conn.Dispose();
                    }
                }
                catch
                {
                    throw;
                }
            }
            return SequenceList;
        }

        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }

        ~JobSequenceDetails()
        {
            Dispose(false);
        }

    }
}
