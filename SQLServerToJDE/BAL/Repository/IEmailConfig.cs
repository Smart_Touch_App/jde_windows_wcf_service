﻿using Models;
using System.Collections.Generic;

namespace BAL.Repository
{
    public interface IEmailConfig
    {
        EmailConfigDetails GetEmailConfig(ConnectionProperties model, int Emailid);
    }
}
