﻿using Models;
using System;

namespace BAL.Repository
{
    public interface ISaleOrderRepository : IDisposable
    {
        string ProcessSaleOrder(ConnectionProperties model);
    }
}
