﻿using Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.Repository
{
    public class SubModuleDetails:ISubModuleDetails
    {
        private SqlConnectionStringBuilder ConnBuilder;
        public SubModuleDetails()
        {
            ConnBuilder = new SqlConnectionStringBuilder();
        }
        //Method added by Vignesh.S
        public string AddUpdateSubModuleDetail(ConnectionProperties model, ModuleDetail SubModule)
        {
            if (model != null)
            {
                try
                {
                    ConnBuilder.DataSource = model.DbDataSource;
                    ConnBuilder.InitialCatalog = model.DbInitialCatalog;
                    if (model.DbIntegratedSecurity == false)
                    {
                        ConnBuilder.UserID = model.DbUserID;
                        ConnBuilder.Password = model.DbPassword;
                    }
                    ConnBuilder.ConnectTimeout = model.DbConnectTimeout;
                    ConnBuilder.IntegratedSecurity = model.DbIntegratedSecurity;

                    using (SqlConnection Conn = new SqlConnection(ConnBuilder.ConnectionString))
                    {
                        using (SqlCommand comm = new SqlCommand("BUSDTA.SP_InsertUpdateModuleDetails", Conn))
                        {
                            comm.CommandType = CommandType.StoredProcedure;
                            comm.Parameters.AddWithValue("@ModuleName", SubModule.SUBModuleName);
                            comm.Parameters.AddWithValue("@JDEInterfaceID",SubModule.JDEInterfaceID );
                            comm.Parameters.AddWithValue("@ErrorStatus", SubModule.ErrorStatus);
                            comm.Parameters.AddWithValue("@ScheduleStatus", SubModule.ScheduleStatus);
                            Conn.Open();
                            int result = comm.ExecuteNonQuery();
                        }
                    }
                }
                catch
                {
                    throw;
                }
            }
            return "";
        }

        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }

        ~SubModuleDetails()
        {
            Dispose(false);
        }
    }
}
