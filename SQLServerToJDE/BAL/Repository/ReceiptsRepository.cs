﻿using Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.Repository
{
     public class ReceiptsRepository:IReceiptsRepository
    {
          private SqlConnectionStringBuilder ConnBuilder;
        public ReceiptsRepository()
        {
            ConnBuilder = new SqlConnectionStringBuilder();
        }
        //Method added by Vignesh.S
        public string ProcessReceipts(ConnectionProperties model)
        {
            if (model != null)
            {
                try
                {
                    ConnBuilder.DataSource = model.DbDataSource;
                    ConnBuilder.InitialCatalog = model.DbInitialCatalog;
                    if (model.DbIntegratedSecurity == false)
                    {
                        ConnBuilder.UserID = model.DbUserID;
                        ConnBuilder.Password = model.DbPassword;
                    }
                    //ConnBuilder.ConnectTimeout = Int32.MaxValue;
                    ConnBuilder.IntegratedSecurity = model.DbIntegratedSecurity;

                    using (var sqlConn = new SqlConnection(ConnBuilder.ConnectionString))
                    {
                        sqlConn.Open();
                        using (SqlCommand cmdreceiptproc = new SqlCommand("dbo.CreateReceipts_R1", sqlConn))
                        {
                            cmdreceiptproc.CommandType = CommandType.StoredProcedure;
                            cmdreceiptproc.CommandTimeout = Int32.Parse(ConnBuilder.ConnectTimeout.ToString());
                            cmdreceiptproc.ExecuteNonQuery();
                            cmdreceiptproc.Dispose();
                        }
                        sqlConn.Close();
                        sqlConn.Dispose();
                    }
                }
                catch
                {
                    throw;
                }
            }
            return "Completed";
        }

        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }

        ~ReceiptsRepository()
        {
            Dispose(false);
        }
    }
}
