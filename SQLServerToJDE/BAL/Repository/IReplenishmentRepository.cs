﻿using Models;
using System;

namespace BAL.Repository
{
    public interface IReplenishmentRepository : IDisposable
    {
        string ProcessReplenishment(ConnectionProperties model);
        string STOTProcessReplenishment(ConnectionProperties model);
        string Update_STOTProcessReplenishment(ConnectionProperties model, string jdeRefSource);
        string InsertAndUpdate_STOTReceipts(ConnectionProperties model, string jdeRefSource);
    }
}
