﻿using Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.Repository
{
    public class UpdateSchedule:IUpdateSchedule
    {
         private SqlConnectionStringBuilder ConnBuilder;
        public UpdateSchedule()
        {
            ConnBuilder = new SqlConnectionStringBuilder();
        }
        //Method added by Vignesh.S
        public string UpdateScheduleDetail(ConnectionProperties model, ModuleDetail UpdateModule)
        {
            if (model != null)
            {
                try
                {
                    ConnBuilder.DataSource = model.DbDataSource;
                    ConnBuilder.InitialCatalog = model.DbInitialCatalog;
                    if (model.DbIntegratedSecurity == false)
                    {
                        ConnBuilder.UserID = model.DbUserID;
                        ConnBuilder.Password = model.DbPassword;
                    }
                    ConnBuilder.ConnectTimeout = model.DbConnectTimeout;
                    ConnBuilder.IntegratedSecurity = model.DbIntegratedSecurity;

                    using (SqlConnection Conn = new SqlConnection(ConnBuilder.ConnectionString))
                    {
                        using (SqlCommand comm = new SqlCommand("BUSDTA.SP_UpdateScheduleNextRunTime", Conn))
                        {
                            comm.CommandType = CommandType.StoredProcedure;
                            comm.Parameters.AddWithValue("@ServiceName", UpdateModule.ServiceName);
                            comm.Parameters.AddWithValue("@JDEInterfaceID", UpdateModule.JDEInterfaceID);
                            comm.Parameters.AddWithValue("@EnvironmentName", UpdateModule.EnvironmentName);
                            comm.Parameters.AddWithValue("@ErrorStatus", UpdateModule.ErrorStatus);
                            comm.Parameters.AddWithValue("@ScheduleStatus", UpdateModule.ScheduleStatus);
                            Conn.Open();
                            int result = comm.ExecuteNonQuery();
                        }
                    }
                }
                catch
                {
                    throw;
                }
            }
            return "";
        }

        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }

        ~UpdateSchedule()
        {
            Dispose(false);
        }
    }
}
