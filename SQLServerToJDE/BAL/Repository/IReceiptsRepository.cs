﻿using Models;

namespace BAL.Repository
{
    public interface IReceiptsRepository
    {
        string ProcessReceipts(ConnectionProperties model);
    }
}
