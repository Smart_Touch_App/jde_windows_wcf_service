﻿using Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace BAL.Repository
{
   public class EmailConfig:IEmailConfig
    {
       private SqlConnectionStringBuilder ConnBuilder;
        public EmailConfig()
        {
            ConnBuilder = new SqlConnectionStringBuilder();
        }

       //Method added by Vignesh.S
        public EmailConfigDetails GetEmailConfig(ConnectionProperties model, int Emailid)
        {

            EmailConfigDetails EmailConfigDetail = new EmailConfigDetails();
            SqlDataReader sqlDatardrHeader = null;
            if (model != null)
            {
                try
                {
                    ConnBuilder.DataSource = model.DbDataSource;
                    ConnBuilder.InitialCatalog = model.DbInitialCatalog;
                    if (model.DbIntegratedSecurity == false)
                    {
                        ConnBuilder.UserID = model.DbUserID;
                        ConnBuilder.Password = model.DbPassword;
                    }
                    ConnBuilder.ConnectTimeout = model.DbConnectTimeout;
                    ConnBuilder.IntegratedSecurity = model.DbIntegratedSecurity;

                    using (SqlConnection Conn = new SqlConnection(ConnBuilder.ConnectionString))
                    {
                        using (SqlCommand comm = new SqlCommand("BUSDTA.SP_GetEmailConfig", Conn))
                        {
                            comm.CommandType = CommandType.StoredProcedure;
                            comm.Parameters.AddWithValue("@EmailID", Emailid);
                            Conn.Open();
                            sqlDatardrHeader= comm.ExecuteReader();
                           while (sqlDatardrHeader.Read())
                           {

                               EmailConfigDetail.EmailConfigID = int.Parse(sqlDatardrHeader["ID"].ToString());
                               EmailConfigDetail.GroupEmailName = sqlDatardrHeader["GroupEmailName"].ToString();
                               EmailConfigDetail.FromRecipients = sqlDatardrHeader["FromRecipients"].ToString();
                               EmailConfigDetail.ToRecipients = sqlDatardrHeader["ToRecipients"].ToString();
                               EmailConfigDetail.CcRecipients = sqlDatardrHeader["CcRecipients"].ToString();
                               EmailConfigDetail.password = sqlDatardrHeader["Password"].ToString();
                               EmailConfigDetail.BccRecipients = sqlDatardrHeader["BccRecipients"].ToString();
                           }
                        }
                        Conn.Close();
                        Conn.Dispose();
                    }
                }
                catch
                {
                    throw;
                }
            }
            return EmailConfigDetail;
        }

        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }

        ~EmailConfig()
        {
            Dispose(false);
        }
    }
}
