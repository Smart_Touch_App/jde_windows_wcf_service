﻿using Models;
using System;
using System.Data;
using System.Data.SqlClient;

namespace BAL.Repository
{
    public class MetricsData : IMetricsData
    {
        private SqlConnectionStringBuilder ConnBuilder;

        public MetricsData()
        {
            ConnBuilder = new SqlConnectionStringBuilder();
        }

        //Code Added by Vignesh.S
        public string InsertMetricData(ConnectionProperties model)
        {
            int count = 0;
            if (model != null)
            {
                try
                {
                    if (model != null)
                    {
                        try
                        {
                            ConnBuilder.DataSource = model.DbDataSource;
                            ConnBuilder.InitialCatalog = model.DbInitialCatalog;
                            if (model.DbIntegratedSecurity == false)
                            {
                                ConnBuilder.UserID = model.DbUserID;
                                ConnBuilder.Password = model.DbPassword;
                            }
                            ConnBuilder.IntegratedSecurity = model.DbIntegratedSecurity;
                            using (var sqlConn = new SqlConnection(ConnBuilder.ConnectionString))
                            {
                                sqlConn.Open();
                                using (SqlCommand cmdreceiptproc = new SqlCommand("BUSDTA.SP_GenerateMetric", sqlConn))
                                {
                                    cmdreceiptproc.CommandType = CommandType.StoredProcedure;
                                    cmdreceiptproc.CommandTimeout = 0;
                                    cmdreceiptproc.ExecuteNonQuery();
                                    cmdreceiptproc.Dispose();
                                }
                                sqlConn.Close();
                                sqlConn.Dispose();
                            }
                        }
                        catch
                        {
                            throw;
                        }
                    }
                    return "Completed";
                }
                catch
                {
                    throw;
                }
            }
            return count != 0 ? "Completed" : "Error";
        }

        public string InsertDeliveryCycles(ConnectionProperties model)
        {
            int count = 0;
            if (model != null)
            {
                try
                {
                    if (model != null)
                    {
                        try
                        {
                            ConnBuilder.DataSource = model.DbDataSource;
                            ConnBuilder.InitialCatalog = model.DbInitialCatalog;
                            if (model.DbIntegratedSecurity == false)
                            {
                                ConnBuilder.UserID = model.DbUserID;
                                ConnBuilder.Password = model.DbPassword;
                            }
                            ConnBuilder.IntegratedSecurity = model.DbIntegratedSecurity;
                            using (var sqlConn = new SqlConnection(ConnBuilder.ConnectionString))
                            {
                                sqlConn.Open();
                                using (SqlCommand cmdreceiptproc = new SqlCommand("BUSDTA.SP_GenerateDeliveryCycles", sqlConn))
                                {
                                    cmdreceiptproc.CommandType = CommandType.StoredProcedure;
                                    cmdreceiptproc.CommandTimeout = 0;
                                    cmdreceiptproc.ExecuteNonQuery();
                                    cmdreceiptproc.Dispose();
                                }
                                sqlConn.Close();
                                sqlConn.Dispose();
                            }
                        }
                        catch
                        {
                            throw;
                        }
                    }
                    return "Completed";
                }
                catch
                {
                    throw;
                }
            }
            return count != 0 ? "Completed" : "Error";
        }

        


        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }

        ~MetricsData()
        {
            Dispose(false);
        }

    }
}
