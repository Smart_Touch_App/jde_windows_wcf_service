﻿using Models;
using System.Collections.Generic;

namespace BAL.Repository
{
    public interface IJobSequenceDetails
    {
        List<ModuleSequence> GetSequenceList(ConnectionProperties model, int jodid);
    }
}
