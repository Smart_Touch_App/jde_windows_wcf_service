﻿using Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.Repository
{
    public class GetScheduleDetails : IGetScheduleDetails
    {
        private SqlConnectionStringBuilder ConnBuilder;

        public GetScheduleDetails()
        {
            ConnBuilder = new SqlConnectionStringBuilder();
        }
        //Method added by Vignesh.S
        public List<ConditionalParameters> GetScheduleList(ConnectionProperties model, ConditionalParameters Schedule)
        {

            List<ConditionalParameters> ScheduleList = new List<ConditionalParameters>();
            SqlDataReader sqlDatardrHeader = null;
            if (model != null)
            {
                try
                {
                    ConnBuilder.DataSource = model.DbDataSource;
                    ConnBuilder.InitialCatalog = model.DbInitialCatalog;
                    if (model.DbIntegratedSecurity == false)
                    {
                        ConnBuilder.UserID = model.DbUserID;
                        ConnBuilder.Password = model.DbPassword;
                    }
                    ConnBuilder.ConnectTimeout = model.DbConnectTimeout;
                    ConnBuilder.IntegratedSecurity = model.DbIntegratedSecurity;

                    using (SqlConnection Conn = new SqlConnection(ConnBuilder.ConnectionString))
                    {
                        using (SqlCommand comm = new SqlCommand("BUSDTA.SP_GetScheduleDetails", Conn))
                        {
                            comm.CommandType = CommandType.StoredProcedure;
                            comm.Parameters.AddWithValue("@ServiceName", Schedule.Servicename);
                            Conn.Open();
                            sqlDatardrHeader= comm.ExecuteReader();
                           while (sqlDatardrHeader.Read())
                           {
                               ConditionalParameters scheduler = new ConditionalParameters();
                               scheduler.Servicename = Schedule.Servicename;
                               scheduler.JobID =int.Parse(sqlDatardrHeader["JobID"].ToString());
                               scheduler.EnvironmentName=sqlDatardrHeader["EnvironmentName"].ToString();
                               scheduler.JDEInterfaceID = sqlDatardrHeader["JDEInterfaceID"].ToString();
                               scheduler.ScheduleID = int.Parse(sqlDatardrHeader["ScheduleID"].ToString());
                               scheduler.IsImmediate =(bool)sqlDatardrHeader["IsImmediate"];
                               ScheduleList.Add(scheduler);
                           }
                        }
                        Conn.Close();
                        Conn.Dispose();
                    }
                }
                catch 
                {
                    throw;
                }
            }
            return ScheduleList;
        }


        public List<string> GetEnvironmentList(ConnectionProperties model)
        {
            List<string> EnvironmentList = new List<string>();
            SqlDataReader sqlDatardrHeader = null;
            if (model != null)
            {
                try
                {
                    ConnBuilder.DataSource = model.DbDataSource;
                    ConnBuilder.InitialCatalog = model.DbInitialCatalog;
                    if (model.DbIntegratedSecurity == false)
                    {
                        ConnBuilder.UserID = model.DbUserID;
                        ConnBuilder.Password = model.DbPassword;
                    }
                    ConnBuilder.ConnectTimeout = model.DbConnectTimeout;
                    ConnBuilder.IntegratedSecurity = model.DbIntegratedSecurity;
                    string Envname = "";
                    using (SqlConnection Conn = new SqlConnection(ConnBuilder.ConnectionString))
                    {
                        using (SqlCommand comm = new SqlCommand("BUSDTA.SP_GetEnvironmentDetails", Conn))
                        {
                            comm.CommandType = CommandType.StoredProcedure;
                            Conn.Open();
                            sqlDatardrHeader = comm.ExecuteReader();
                            while (sqlDatardrHeader.Read())
                            {
                                Envname = sqlDatardrHeader["EnvironmentName"].ToString();
                                EnvironmentList.Add(Envname);
                            }
                        }
                        Conn.Close();
                        Conn.Dispose();
                    }
                }
                catch
                {
                    throw;
                }
            }
            return EnvironmentList;
        }

        public ConnectionProperties Getconnection(ConnectionProperties model, string EnvName)
        {
            ConnectionProperties ConnectionProp = new ConnectionProperties();
            SqlDataReader sqlDatardrHeader = null;
            if (model != null)
            {
                try
                {
                    ConnBuilder.DataSource = model.DbDataSource;
                    ConnBuilder.InitialCatalog = model.DbInitialCatalog;
                    if (model.DbIntegratedSecurity == false)
                    {
                        ConnBuilder.UserID = model.DbUserID;
                        ConnBuilder.Password = model.DbPassword;
                    }
                    ConnBuilder.ConnectTimeout = model.DbConnectTimeout;
                    ConnBuilder.IntegratedSecurity = model.DbIntegratedSecurity;
                    string con = string.Empty;
                    using (SqlConnection Conn = new SqlConnection(ConnBuilder.ConnectionString))
                    {
                        using (SqlCommand comm = new SqlCommand("SELECT PortalConnectionString FROM [SystemDB].[BUSDTA].[Environment_Master] where EnvName='" + EnvName + "' ", Conn))
                        {
                            comm.CommandType = CommandType.StoredProcedure;
                            Conn.Open();
                            con = comm.ExecuteScalar().ToString();
                            
                        }
                        Conn.Close();
                        Conn.Dispose();
                    }
                    SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(con);

                }
                catch
                {
                    throw;
                }
            }
            return ConnectionProp;
        }


        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }

        ~GetScheduleDetails()
        {
            Dispose(false);
        }



    

    }
}
