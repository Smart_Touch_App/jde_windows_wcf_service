﻿using Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.Repository
{
    public class ServiceHistoryDetail:IServiceHistoryDetail
    {
         private SqlConnectionStringBuilder ConnBuilder;

        public ServiceHistoryDetail()
        {
            ConnBuilder = new SqlConnectionStringBuilder();
        }

        //Code Added by Marimuthu
        public string InsertServiceHistory(ConnectionProperties model, ServiceHistory Servicedetails)
        {
            int count = 0;
            if (model != null)
            {
                try
                {
                    ConnBuilder.DataSource = model.DbDataSource;
                    ConnBuilder.InitialCatalog = model.DbInitialCatalog;
                    if (model.DbIntegratedSecurity == false)
                    {
                        ConnBuilder.UserID = model.DbUserID;
                        ConnBuilder.Password = model.DbPassword;
                    }
                    ConnBuilder.ConnectTimeout = Int32.MaxValue;
                    ConnBuilder.IntegratedSecurity = model.DbIntegratedSecurity;

                    using (SqlConnection Conn = new SqlConnection(ConnBuilder.ConnectionString))
                    {
                        using (SqlCommand comm = new SqlCommand("BUSDTA.SP_InsertServiceHistory", Conn))
                        {
                            comm.CommandType = CommandType.StoredProcedure;
                            comm.Parameters.AddWithValue("@JobID", Servicedetails.JobID);
                            comm.Parameters.AddWithValue("@EnvironmentName", Servicedetails.EnvironmentName);
                            comm.Parameters.AddWithValue("@ScheduleID", Servicedetails.ScheduleID);
                            comm.Parameters.AddWithValue("@Status", Servicedetails.Status);
                            Conn.Open();
                            count=comm.ExecuteNonQuery();
                        }
                        Conn.Close();
                        Conn.Dispose();
                    }
                }
                catch
                {
                    throw;
                }
            }
            return count != 0 ? "Completed" : "Error";
        }

        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }

        ~ServiceHistoryDetail()
        {
            Dispose(false);
        }
    }
}
