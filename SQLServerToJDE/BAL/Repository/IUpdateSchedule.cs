﻿using Models;

namespace BAL.Repository
{
    public interface IUpdateSchedule
    {
        string UpdateScheduleDetail(ConnectionProperties model, ModuleDetail UpdateModule);
    }
}
