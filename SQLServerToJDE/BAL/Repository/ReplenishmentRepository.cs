﻿using Models;
using System;
using System.Data;
using System.Data.SqlClient;

namespace BAL.Repository
{
    public class ReplenishmentRepository : IReplenishmentRepository
    {
        private SqlConnectionStringBuilder ConnBuilder;
        public ReplenishmentRepository()
        {
            ConnBuilder = new SqlConnectionStringBuilder();
        }
        //Method added by Vignesh.S
        public string ProcessReplenishment(ConnectionProperties model)
        {
            if (model != null)
            {
                try
                {
                    ConnBuilder.DataSource = model.DbDataSource;
                    ConnBuilder.InitialCatalog = model.DbInitialCatalog;
                    if (model.DbIntegratedSecurity == false)
                    {
                        ConnBuilder.UserID = model.DbUserID;
                        ConnBuilder.Password = model.DbPassword;
                    }
                    //ConnBuilder.ConnectTimeout = Int32.MaxValue;
                    ConnBuilder.IntegratedSecurity = model.DbIntegratedSecurity;

                    using (SqlConnection Conn = new SqlConnection(ConnBuilder.ConnectionString))
                    {
                        using (SqlCommand comm = new SqlCommand("BUSDTA.SP_InsertAndUpdate_ReplenishmentProcessor", Conn))
                        {
                            comm.CommandType = CommandType.StoredProcedure;
                            comm.Parameters.AddWithValue("@jdeLinkSource", model.JDEWriteLinkSource);
                            Conn.Open();
                            int result = comm.ExecuteNonQuery();
                        }

                    }
                }
                catch
                {
                    throw;
                }
            }
            return "Completed";
        }


        public string STOTProcessReplenishment(ConnectionProperties model)
        {
            if (model != null)
            {
                try
                {
                    ConnBuilder.DataSource = model.DbDataSource;
                    ConnBuilder.InitialCatalog = model.DbInitialCatalog;
                    if (model.DbIntegratedSecurity == false)
                    {
                        ConnBuilder.UserID = model.DbUserID;
                        ConnBuilder.Password = model.DbPassword;
                    }
                    //ConnBuilder.ConnectTimeout = Int32.MaxValue;
                    ConnBuilder.IntegratedSecurity = model.DbIntegratedSecurity;

                    using (SqlConnection Conn = new SqlConnection(ConnBuilder.ConnectionString))
                    {
                        using (SqlCommand comm = new SqlCommand("BUSDTA.SP_InsertAndUpdate_STOTReplenishmentProcessor", Conn))
                        {
                            comm.CommandType = CommandType.StoredProcedure;
                            comm.Parameters.AddWithValue("@jdeLinkSource", model.JDEWriteLinkSource);
                            Conn.Open();
                            int result = comm.ExecuteNonQuery();
                        }

                    }
                }
                catch
                {
                    throw;
                }
            }
            return "Completed";
        }

        public string Update_STOTProcessReplenishment(ConnectionProperties model, string jdeRefSource)
        {
            if (model != null)
            {
                try
                {
                    ConnBuilder.DataSource = model.DbDataSource;
                    ConnBuilder.InitialCatalog = model.DbInitialCatalog;
                    if (model.DbIntegratedSecurity == false)
                    {
                        ConnBuilder.UserID = model.DbUserID;
                        ConnBuilder.Password = model.DbPassword;
                    }
                    //ConnBuilder.ConnectTimeout = Int32.MaxValue;
                    ConnBuilder.IntegratedSecurity = model.DbIntegratedSecurity;

                    using (SqlConnection Conn = new SqlConnection(ConnBuilder.ConnectionString))
                    {
                        using (SqlCommand comm = new SqlCommand("BUSDTA.SP_Update_STOTReplenishmentStatus", Conn))
                        {
                            comm.CommandType = CommandType.StoredProcedure;
                            comm.Parameters.AddWithValue("@jdeLinkSource", model.JDEWriteLinkSource);
                            comm.Parameters.AddWithValue("@jdeRefSource", jdeRefSource);
                            Conn.Open();
                            int result = comm.ExecuteNonQuery();
                        }

                    }
                }
                catch
                {
                    throw;
                }
            }
            return "Completed";
        }
        public string InsertAndUpdate_STOTReceipts(ConnectionProperties model, string jdeRefSource)
        {
            if (model != null)
            {
                try
                {
                    ConnBuilder.DataSource = model.DbDataSource;
                    ConnBuilder.InitialCatalog = model.DbInitialCatalog;
                    if (model.DbIntegratedSecurity == false)
                    {
                        ConnBuilder.UserID = model.DbUserID;
                        ConnBuilder.Password = model.DbPassword;
                    }
                    //ConnBuilder.ConnectTimeout = Int32.MaxValue;
                    ConnBuilder.IntegratedSecurity = model.DbIntegratedSecurity;

                    using (SqlConnection Conn = new SqlConnection(ConnBuilder.ConnectionString))
                    {
                        using (SqlCommand comm = new SqlCommand("BUSDTA.SP_InsertAndUpdate_STOTReceipts", Conn))
                        {
                            comm.CommandType = CommandType.StoredProcedure;
                            comm.Parameters.AddWithValue("@jdeLinkSource", model.JDEWriteLinkSource);
                            comm.Parameters.AddWithValue("@jdeRefSource", jdeRefSource);
                            Conn.Open();
                            int result = comm.ExecuteNonQuery();
                        }

                    }
                }
                catch
                {
                    throw;
                }
            }
            return "Completed";
        }

        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }

        ~ReplenishmentRepository()
        {
            Dispose(false);
        }
    }
}
