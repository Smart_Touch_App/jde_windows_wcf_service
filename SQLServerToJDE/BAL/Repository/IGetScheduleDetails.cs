﻿using System;
using Models;
using System.Collections.Generic;

namespace BAL.Repository
{
    public interface IGetScheduleDetails : IDisposable
    {
         List<ConditionalParameters> GetScheduleList(ConnectionProperties model, ConditionalParameters Schedule);
         List<string> GetEnvironmentList(ConnectionProperties model);
         ConnectionProperties Getconnection(ConnectionProperties model, string EnvName);
    }
}
