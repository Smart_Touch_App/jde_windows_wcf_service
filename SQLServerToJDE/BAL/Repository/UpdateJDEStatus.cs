﻿using Models;
using System;
using System.Data;
using System.Data.SqlClient;

namespace BAL.Repository
{
    public class UpdateJDEStatus:IUpdateJDEStatus
    {
         private SqlConnectionStringBuilder ConnBuilder;

        public UpdateJDEStatus()
        {
            ConnBuilder = new SqlConnectionStringBuilder();
        }
        //Method added by Vignesh.S
        public int UpdateJDEStatusDetail(ConnectionProperties model)
        {
           int result=0;
            if (model != null)
            {
                try
                {
                    ConnBuilder.DataSource = model.DbDataSource;
                    ConnBuilder.InitialCatalog = model.DbInitialCatalog;
                    if (model.DbIntegratedSecurity == false)
                    {
                        ConnBuilder.UserID = model.DbUserID;
                        ConnBuilder.Password = model.DbPassword;
                    }
                    ConnBuilder.ConnectTimeout = Int32.MaxValue;
                    ConnBuilder.IntegratedSecurity = model.DbIntegratedSecurity;

                    using (SqlConnection Conn = new SqlConnection(ConnBuilder.ConnectionString))
                    {
                        using (SqlCommand comm = new SqlCommand("BUSDTA.SP_UpdateJDEStatus", Conn))
                        {
                            Conn.Open();
                           result = comm.ExecuteNonQuery();
                          
                        }
                        Conn.Close();
                        Conn.Dispose();
                    }
                }
                catch
                {
                    throw;
                }
            }
            return result;
        }

        public int UpdateVarianceStatusDetail(ConnectionProperties model)
        {
            int result = 0;
            if (model != null)
            {
                try
                {
                    ConnBuilder.DataSource = model.DbDataSource;
                    ConnBuilder.InitialCatalog = model.DbInitialCatalog;
                    if (model.DbIntegratedSecurity == false)
                    {
                        ConnBuilder.UserID = model.DbUserID;
                        ConnBuilder.Password = model.DbPassword;
                    }
                    ConnBuilder.ConnectTimeout = Int32.MaxValue;
                    ConnBuilder.IntegratedSecurity = model.DbIntegratedSecurity;

                    using (SqlConnection Conn = new SqlConnection(ConnBuilder.ConnectionString))
                    {
                        using (SqlCommand comm = new SqlCommand("BUSDTA.SP_UpdateVarianceStatus", Conn))
                        {
                            Conn.Open();
                            result = comm.ExecuteNonQuery();

                        }
                        Conn.Close();
                        Conn.Dispose();
                    }
                }
                catch
                {
                    throw;
                }
            }
            return result;
        }
        bool disposed = false;
        System.Runtime.InteropServices.SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }

        ~UpdateJDEStatus()
        {
            Dispose(false);
        }

    }
}
