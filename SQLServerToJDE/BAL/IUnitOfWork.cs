﻿using BAL.Repository;
using System;

namespace BAL
{
    public interface IUnitOfWork : IDisposable
    {
        IReplenishmentRepository SampleRepository { get; }
        ISaleOrderRepository SaleOrderRepository { get; }
        IGetScheduleDetails GetScheduleList { get; }
        ISubModuleDetails GetSubModuleDetail { get; }
        IUpdateSchedule UpdateScheduleDetail { get; }
        IReceiptsRepository ReceiptRepository { get;}
        IEmailConfig EmailConfigDetail { get; }
        IJobSequenceDetails JobSequenceList { get; }
        IServiceHistoryDetail ServiceHistory { get; }
        IUpdateJDEStatus UpdateJDEStatus { get; }
        IMetricsData InsertMetricsData { get; }
        IInventoryDetail InsertInventoryDetail { get; }

    }
}
