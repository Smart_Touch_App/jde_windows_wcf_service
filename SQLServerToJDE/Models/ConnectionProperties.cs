﻿
using System;
//Models created by Marimuthu
namespace Models
{
    public class ConnectionProperties
    {
        public string DbDataSource { get; set; }
        public string DbInitialCatalog { get; set; }
        public string DbUserID { get; set; }
        public string DbPassword { get; set; }
        public int DbConnectTimeout { get; set; }
        public bool DbIntegratedSecurity { get; set; }
        public string JDEWriteLinkSource { get; set; }
    }
    public class ConditionalParameters
    {
        public string EnvironmentName { get; set; }
        public int JobID { get; set; }
        public int ScheduleID { get; set; }
        public string Servicename { get; set; }
        public string JDEInterfaceID{ get; set; }
        public bool IsImmediate { get; set; }
    }
    public class ModuleDetail
    {
        public string SUBModuleName { get; set; }
        public string EnvironmentName { get; set; }
        public string JDEInterfaceID { get; set; }
        public string ErrorStatus { get; set; }
        public string ServiceName { get; set; }
        public string ScheduleStatus { get; set; }
    }

    public class ServiceHistory
    {
        public string EnvironmentName { get; set; }
        public int JobID { get; set; }
        public int ScheduleID { get; set; }
        public string Status { get; set; }
    }
    public class ModuleSequence
    {
        public int JobID { get; set; }
        public string JobName { get; set; }
        public string ModuleName { get; set; }
        public int SequenceNo { get; set; }
        public bool Dependency { get; set; }
        public bool EmailNotification { get; set; }
        public int EmailConfigID { get; set; }
        
    }
    public class EmailConfigDetails
    {
        public int EmailConfigID { get; set; }
        public string GroupEmailName { get; set; }
        public string FromRecipients { get; set; }
        public string password { get; set; }
        public string ToRecipients { get; set; }
        public string CcRecipients { get; set; }
        public string BccRecipients { get; set; }
    }
    public class SettlementDetails
    {
        public string Routeid { get; set; }
        public string Fromdate { get; set; }
        public string Todate { get; set; }

    }
}
